//
//  StickerImageView.swift
//  SampleStickerToVideo
//
//  Created by apple on 14/11/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation
import UIKit

class StickerImageView:UIImageView {
    
    var updatedFrame:CGRect!
    var trashButton:UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        addAllGestureRecognizers()
        addTrashButton()
        trashButton?.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func addTrashButton(){
        trashButton = UIButton()
        trashButton?.frame  = CGRect(x: self.frame.size.width/2, y: -(self.frame.size.height - 100), width: 100, height: 100)
        
        trashButton?.setImage(UIImage(named :"deleteSticker"), for: .normal)
        trashButton?.addTarget(self, action: #selector(removeSticker(_:)), for: .touchUpInside)
        trashButton?.isUserInteractionEnabled = true
        self.addSubview(trashButton!)
    }
    
    func addAllGestureRecognizers() {
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
        addGestureRecognizer(tapGes)
        
        let longGes = UILongPressGestureRecognizer(target: self, action: #selector(longPressOnSticker(press:)))
        addGestureRecognizer(longGes)
        
        let panGes = UIPanGestureRecognizer(target: self, action: #selector(self.panGesturehandler))
        addGestureRecognizer(panGes)
        
        let rotateGes = UIRotationGestureRecognizer(target: self, action: #selector(self.rotationGesturehandler))
        addGestureRecognizer(rotateGes)
        
        let pinchGes = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGesturehandler))
        addGestureRecognizer(pinchGes)
    }
    
    @objc func tapGestureHandler() {
        superview?.bringSubview(toFront: self as? UIView ?? UIView())
    }
    
    @objc func longGestureHandler() {
      //  removeFromSuperview()

    }
    func longPressOnSticker(press: UILongPressGestureRecognizer!){
        
        press.minimumPressDuration = 0.5
        if press.state == .began{
            print("long press")
            startWiggle(for: self)
        }
    }
    
    @objc func panGesturehandler(_ panGest: UIPanGestureRecognizer) {
        if panGest.state == .began {
            
            superview?.bringSubview(toFront: self as? UIView ?? UIView())
        }
        if panGest.state == .began || panGest.state == .changed {
            let translation: CGPoint = panGest.translation(in: superview)
            center = CGPoint(x: center.x + translation.x, y: center.y + translation.y)
            panGest.setTranslation(CGPoint.zero, in: superview)
        }
        let test: CATransform3D = layer.transform
        let testTransform: CGAffineTransform = transform
        let viewFrame: CGRect = frame
        let viewLayerFrame: CGRect = layer.frame
        print("layer: test:\(test.m11)")
        print("view: testTransform:\(testTransform.a)")
        updatedFrame = viewLayerFrame
    }
    
    @objc func rotationGesturehandler(_ rotationGest: UIRotationGestureRecognizer) {
        if rotationGest.state == .began {
            superview?.bringSubview(toFront: self as? UIView ?? UIView())
        }
        else if rotationGest.state == .changed {
            transform = transform.rotated(by: rotationGest.rotation)
            rotationGest.rotation = 0
        }
        
    }
    
    @objc func pinchGesturehandler(_ pinchGest: UIPinchGestureRecognizer) {
        if pinchGest.state == .began {
            superview?.bringSubview(toFront: self as? UIView ?? UIView())
        }
        else if pinchGest.state == .changed {
            transform = transform.scaledBy(x: pinchGest.scale, y: pinchGest.scale)
            pinchGest.scale = 1.0
        }
    }
    //Wiggle animation to delete sticker
    let wiggleBounceY = 4.0
    let wiggleBounceDuration = 0.12
    let wiggleBounceDurationVariance = 0.025
    let wiggleRotateAngle = 0.06
    let wiggleRotateDuration = 0.10
    let wiggleRotateDurationVariance = 0.025
    
    func randomize(interval: TimeInterval, withVariance variance: Double) -> Double{
        let random = (Double(arc4random_uniform(1000)) - 500.0) / 500.0
        return interval + variance * random
        
    }
    func startWiggle(for view: UIView) {
        
        trashButton?.isHidden = false
        //Create rotation animation
        let rotationAnim = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotationAnim.values = [-wiggleRotateAngle, wiggleRotateAngle]
        rotationAnim.autoreverses = true
        rotationAnim.duration = randomize(interval: wiggleRotateDuration, withVariance: wiggleRotateDurationVariance)
        rotationAnim.repeatCount = HUGE
        
        //Create bounce animation
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        bounceAnimation.values = [wiggleBounceY, 0]
        bounceAnimation.autoreverses = true
        bounceAnimation.duration = randomize(interval: wiggleBounceDuration, withVariance: wiggleRotateDurationVariance)
        bounceAnimation.repeatCount = HUGE
        
        //Apply animations to view
        UIView.animate(withDuration: 0) {
            view.layer.add(rotationAnim, forKey: "rotation")
            view.layer.add(bounceAnimation, forKey: "bounce")
            view.transform = .identity
        }
    }
    func stopWiggle(for view: UIView){
        view.layer.removeAllAnimations()
    }
    @IBAction func removeSticker(_ sender: UIButton) {
        print("action delete")
        
        UIView.animate(withDuration: 0.3, animations:{
            self.removeFromSuperview()
        })
    }
}
