//
//  UIAppViewController.swift
//  Mojilatino
//
//  Created by apple on 09/10/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class UIAppViewController: UIViewController, RequestManagerDelegate{
    
    //shared Instance
    var requestManager = UIAppServerRequests.sharedInstance
    var reachability =  Connectivity.sharedInstance
    var listOfCountries:[String] = []
    
    let keychainWrapper = KeychainWrapper(serviceName: KeychainWrapper.standard.serviceName, accessGroup: "group.myAccessGroup")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigationBar()
        requestManager.delegate = self
        
        listOfCountries = ["United States","Cuba", "Puerto Rico", "Dominican Republic","Mexico","Venezuela","Colombia", "Ecuador","Panama","Honduras","Nicaragua","El Salvador","Guatemala","Chile","Peru","Uruguay","Bolivia","Paraguay","Brazil","Costa Rica"]
    }
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    func hideNavigationBar(){
        self.navigationController?.isNavigationBarHidden =  true
    }
    func removeTokenFromKeyChainWrapper(){
        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "token")
        print("Remove Successful: \(removeSuccessful)")
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
