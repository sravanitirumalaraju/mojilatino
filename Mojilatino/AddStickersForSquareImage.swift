//
//  AddStickersForSquareImage.swift
//  Mojilatino
//
//  Created by apple on 23/10/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit
import AssetsLibrary
import Alamofire
import Kingfisher


class AddStickersForSquareImage: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    var newDrag: UIImage?
    var selected: Int?
    var videoImageUrlFromChangeCountryController : URL?
    var passedImage2: UIImage?
    var searchArray = [String]()
    var selectedType: String?
    
   //sravani
    @IBOutlet var swipeOutlet: UIView!
    @IBOutlet var segmentedStickersOutlet: UISegmentedControl!
    @IBOutlet var saveOutlet: UIButton!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var hideView: UIView!
    @IBOutlet var heightUIView: NSLayoutConstraint!
    @IBOutlet var photoPreviewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var textFieldText: UITextField!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var imageClicked: UIImageView!
    @IBOutlet var viewUp: UIView!
    @IBOutlet weak var canvasView: UIView!
    
   
    var indexTag = 0
    let trashButton = UIButton()
//    let BorderView = CAShapeLayer()
    var dragView = [DraggableStickerView]()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
//        textFieldText.isHidden = true
        self.selectedType = "Normal"  // Initial setup
        self.addInitialUISetup()
//        interstitial = createAndLoadInterstitial()
        canvasView.addSubview(imageClicked)
        
        segmentedStickersOutlet.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        self.segmentedStickersOutlet.addTarget(self, action: #selector(self.segmentedControlForStickers(_ :)), for: .valueChanged)
        
        if passedImage2 != nil{
            print(passedImage2!)
            imageClicked.image = passedImage2
        }
        
        addSwipeGesture()
        addTapGesture()
        swipeOutlet.isHidden = true
        // textFieldText.makeRoundedEdge(boarderWidth: 1.0, borderColour: UIColor.black, cornerRadius: 20.0)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imageClicked.addGestureRecognizer(tap)
        imageClicked.isUserInteractionEnabled = true
        
//        BorderView.strokeColor = UIColor.red.cgColor
//        BorderView.lineDashPattern = [12, 5]
//        BorderView.lineWidth = 2
//        BorderView.fillColor = UIColor.clear.cgColor
//        BorderView.strokeStart = 0
//        BorderView.strokeEnd = 1
        
    }
    //MARK:- function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
//       BorderView.removeFromSuperlayer()
//        BorderView.isHidden = true
        trashButton.isHidden = true
    }
    //MARK:- ADD INITAL UI SETUP
    func addInitialUISetup(){
        self.saveButton.addBlackShadeWithRoundedEdges()
        let myColor = UIColor.black
        self.textFieldText.layer.borderColor = myColor.cgColor
        self.textFieldText.layer.borderWidth = 1.0
        self.textFieldText.layer.cornerRadius = 15
    }
    //MARK:- DOWNLOAD IMAGE FROM WEB
    func downloadImageFromWeb(s : String, imageView: UIImageView){
        print(s)
        let imageUrl = URL(string: s)
        imageView.kf.setImage(with: imageUrl)
    }
    //MARK:- ADD SWIPE GESTURE
    func addSwipeGesture(){
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
        self.swipeOutlet.addGestureRecognizer(swipeGestureRecognizer)
    }
    //MARK:- ADD TAP GESTURE
    func addTapGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(tapAction(tap:)))
        self.swipeOutlet.addGestureRecognizer(tapGestureRecognizer)
    }
    //MARK:- SWIPE ACTION RECOGNIZER
    func swipeAction(swipe: UISwipeGestureRecognizer){
        print("swipe happened")
        UIView.animate(withDuration: 0.3, animations:{
        self.heightUIView.constant = 0 + self.viewUp.frame.size.height
       // self.photoPreviewBottomConstraint.constant = 0
            self.textFieldText.isHidden = false
            self.swipeOutlet.isHidden = true
        })
    }
    //MARK:- TAP ACTION RECOGNIZER
    func tapAction(tap: UISwipeGestureRecognizer){
        print("tap happened")
        UIView.animate(withDuration: 0.3, animations:{
            self.heightUIView.constant = 0 + self.viewUp.frame.size.height
            //  self.photoPreviewBottomConstraint.constant = 0 + self.viewUp.frame.size.height
            self.textFieldText.isHidden = false
            self.swipeOutlet.isHidden = true
        })
    }
//    //MARK:- VIEW WILL APPEAR
//    override func viewWillAppear(_ animated: Bool) {
//
//    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //MARK:-textfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder
        UIView.animate(withDuration: 0.50, animations: {
            self.heightUIView.constant = 250
            //    self.viewUp.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.50, animations: {
            //Set x position what ever you want
            textField.resignFirstResponder()
            self.heightUIView.constant = 0
          }, completion: nil)
    }
    
    //MARK:-collection view delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch selectedType {
            case "Normal"?:
                print(imagesArrayNormal)
                if imagesArrayNormal.count == 0{
                    textFieldText.placeholder = "No stickers Found"
                }else{
                    textFieldText.placeholder = "Stickers"
                }
               return imagesArrayNormal.count
            case "Business"?:
                print(imagesArrayBussiness)
                if imagesArrayBussiness.count == 0{
                    textFieldText.placeholder = "No stickers Found"
                }else{
                    textFieldText.placeholder = "Stickers"
                }
                return imagesArrayBussiness.count
            case "Search"?:
                return searchArray.count
            default:
               return imagesArrayNormal.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCellSquareImage", for: indexPath) as! CollectionViewCellForSquareImage
        switch selectedType {
        case "Normal"?:
            print(imagesArrayNormal)
            downloadImageFromWeb(s: imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("normal stickers loaded")
        case "Business"?:
            print(imagesArrayBussiness)
            downloadImageFromWeb(s: imagesArrayBussiness[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("business stickers loaded")
        case "Search"?:
            downloadImageFromWeb(s: searchArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("search stickers loaded")
            
        default:
            self.downloadImageFromWeb(s:
                (imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: cell.myImageView)
        }
        let widthInPixels = cell.myImageView.frame.width * UIScreen.main.scale
        let heightInPixels = cell.myImageView.frame.height * UIScreen.main.scale
        print(cell.myImageView)
        print("width: \(widthInPixels) + heightInPixels : \(heightInPixels)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: self.view.frame.size.width/5, height: self.view.frame.size.width/5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        textFieldText.isHidden = true
        indexTag = indexPath.row
        _ = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCellSquareImage", for: indexPath) as! CollectionViewCellForSquareImage
        stickerAdded()
    }
    //MARK:- sticker added to image
    func stickerAdded(){
        UIView.animate(withDuration: 0.50, animations: {
            self.swipeOutlet.isHidden = false
            self.heightUIView.constant = (self.heightUIView.constant + 1) - (self.viewUp.frame.size.height)
            self.textFieldText.resignFirstResponder()
        }, completion: nil)
        
        let draggableview = DraggableStickerView()
        draggableview.frame =  CGRect(x: 100, y: 100, width: 120, height: 150)
        draggableview.isUserInteractionEnabled = true
        draggableview.backgroundColor = UIColor.clear
        dragView.append(draggableview)
        self.imageClicked.addSubview(draggableview)
        
        let selectedSticker = UIImageView()
        selectedSticker.frame =  CGRect(x: 0, y: 30, width: 120, height: 120)
        selectedSticker.isUserInteractionEnabled = true
        selectedSticker.isMultipleTouchEnabled = true
        draggableview.addSubview(selectedSticker)
        newDrag = selectedSticker.toImage()
        selectedSticker.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(stickerTapped(_:))))
        
//        BorderView.isHidden = false
//        BorderView.frame = selectedSticker.bounds
//        BorderView.path = UIBezierPath(rect: BorderView.bounds).cgPath
//        selectedSticker.layer.addSublayer(BorderView)
        
        trashButton.isHidden = false
        trashButton.frame  = CGRect(x: -15, y: -15, width: 40, height: 40)
        trashButton.setImage(UIImage(named :"deleteSticker"), for: .normal)
//        trashButton.setImage(UIImage(named :"cancelImage0"), for: .normal)
        trashButton.isUserInteractionEnabled = false
        selectedSticker.addSubview(trashButton)
        
        let cancelBtn = UIButton()
        cancelBtn.frame = CGRect(x: -20, y: -20, width: 50, height: 50)
        cancelBtn.tag = dragView.endIndex - 1
        cancelBtn.setImage(UIImage(named :""), for: .normal)
        cancelBtn.isUserInteractionEnabled = true
//        cancelBtn.backgroundColor = UIColor.green
        cancelBtn.addTarget(self, action: #selector(removeSticker(_:)), for: .touchUpInside)
        selectedSticker.addSubview(cancelBtn)
        
        switch selectedType {
        case "Normal"?:
            downloadImageFromWeb(s: imagesArrayNormal[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: selectedSticker)
        case "Business"?:
            downloadImageFromWeb(s: imagesArrayBussiness[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: selectedSticker)
        case "Search"?:
            self.downloadImageFromWeb(s: (searchArray[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: selectedSticker)
        default:
            self.downloadImageFromWeb(s: (imagesArrayNormal[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: selectedSticker)
        }
    }
    //MARK:- remove sticker tapped
    func removeSticker(_ sender: UIButton) {
        print("action delete")
        dragViewTapped(dragView[sender.tag])
    }
    func dragViewTapped(_ sender: DraggableStickerView){
        sender.removeFromSuperview()
    }
    //MARK:- Sticker tapped
    func stickerTapped(_ sender: UITapGestureRecognizer){
        let tappedImageView = sender.view!
//        if BorderView.isHidden == false{
//            BorderView.isHidden = true
//            trashButton.isHidden = true
//        }else{
//            BorderView.isHidden = false
            trashButton.isHidden = false
//
//            BorderView.frame = tappedImageView.bounds
//            BorderView.path = UIBezierPath(rect: BorderView.bounds).cgPath
//            tappedImageView.layer.addSublayer(BorderView)
            trashButton.frame  = CGRect(x: -15, y: -15, width: 40, height: 40)
            tappedImageView.addSubview(trashButton)
//        }
    }
    
    //MARK:- SEGMENT CONTROL FOR STICKERS
    func segmentedControlForStickers(_ sender: UISegmentedControl){
       switch segmentedStickersOutlet.selectedSegmentIndex{
        case 0:
            selectedType = "Normal"
            self.collectionView.reloadData()
        case 1:
            selectedType = "Business"
            self.collectionView.reloadData()
       default:
           selectedType = "Normal"
           self.collectionView.reloadData()
        }
        
    }
    func video(_ video: AVAsset, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        let alert = UIAlertController(title: "Video Saved", message: "Video successfully saved to Photos library", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
            self.present(viewController, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func image(_ image: UIImage, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        let alert = UIAlertController(title: "Image Saved", message: "Image successfully saved to Photos library", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
            self.present(viewController, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- NEXT MONTH PRESSED
    @IBAction func nextButtonPressed(_ sender: Any) {
        if passedImage2 != nil{
//            if interstitial.isReady {
//                interstitial.present(fromRootViewController: self)
//            } else {
                //If there are no adds to show, then save the image
//            BorderView.strokeColor = UIColor.clear.cgColor
//            BorderView.removeFromSuperlayer()
            trashButton.removeFromSuperview()
            UIImageWriteToSavedPhotosAlbum(canvasView.toImage(),self, #selector(AddStickersForSquareImage.image(_:withPotentialError:contextInfo:)), nil)
//            }
        }
    }
    //MARK:- BACK BUTTON PRESSED
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
   
    func requestError(error: Error?) {
        self.view.hideToastActivity()
        self.view.makeToast(message: (error?.localizedDescription)!)
    }
    //MARK:- TEXTFIELD DID CHANGE
    @IBAction func textFieldDidChange(_ sender: Any) {
         if segmentedStickersOutlet.selectedSegmentIndex == 0{
            searchArray.removeAll()
            for (_, element) in normalStickerArray.enumerated(){
                if (element["tag"] as! String).hasPrefix(textFieldText.text!){
                    searchArray.append(element["imgurl"] as! String)
                    //  collectionView.reloadData()
                }
            }
         }else if segmentedStickersOutlet.selectedSegmentIndex == 1{
            searchArray.removeAll()
            for (_, element) in businessStickerArray.enumerated(){
                if (element["tag"] as! String).hasPrefix(textFieldText.text!){
                    searchArray.append(element["imgurl"] as! String)
                  //  collectionView.reloadData()
                }
            }
          }
         selectedType = "Search"
         collectionView.reloadData()
    }
}



