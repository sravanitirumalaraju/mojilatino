//
//  ChangeCountryViewController.swift
//  Mojilatino
//
//  Created by apple on 24/08/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import AVKit

class ChangeCountryViewController: UIAppViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var imageFromPreviousViewController : UIImage?
    var videoUrlpassed: URL?
    var thumbnailVideoImage: UIImage?
    //    var selectedState: Bool? = nil
    var selectedIndex: Int?
    var selectedCountry: String?
    
    @IBOutlet var passedVideo: UIView!
    @IBOutlet var passedImage: UIImageView!
    @IBOutlet var buttonLabel: EZButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var changeCountry: UILabel!
    @IBOutlet var dropDown: UIPickerView!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        addInitialChanges()
        if imageFromPreviousViewController != nil{
            passedImage.image = imageFromPreviousViewController
        }else if videoUrlpassed != nil{
            passedImage.image = thumbnailVideoImage
        }
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        addInitialChanges()
    }
    //MARK:- ADD INITIAL CHANGES
    func addInitialChanges(){
        requestManager.delegate = self
        self.nextButton.addBlackShadeWithRoundedEdges()
        changeCountry.layer.cornerRadius = 15.0
        changeCountry.clipsToBounds = true
        buttonLabel.setTitleColor(UIColor.white, for: .normal)
    }
    //MARK:- SEND COUNTRY NAME
    func sendCountryName(country : String?){
        //network reachability
//        if reachability.isNetworkAvailable {
//            //Api-post
//            self.requestManager.sendCountryData(country: country!)
//            self.view.makeToastActivity(message: "Loading..")
//        }else{
//            self.networkUnavailable()
//        }
        self.requestManager.sendCountryData(country: country!)
    }
    //MARK:-picker delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listOfCountries.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listOfCountries[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountry = listOfCountries[row]
        self.changeCountry.text = listOfCountries[row]
        selectedIndex = row
        self.dropDown.isHidden = true
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = listOfCountries[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.white])
        return myTitle
    }
    //MARK:-BUTTON PRESSED
    @IBAction func buttonPressed(_ sender: Any) {
        dropDown.isHidden = false
    }
    //MARK:- BACK BUTTON CLICKED
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:-RequestManagerDelegate
    func showStickers(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.view.hideToastActivity()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController: AddStickersViewController = storyboard.instantiateViewController(withIdentifier :"addStickersViewController") as!  AddStickersViewController
            viewController.videoImageUrlFromChangeCountryController = self.videoUrlpassed
            viewController.passedImage2 = self.passedImage.image
            self.present(viewController, animated: true)
        }
    }
    
    func requestError(error: Error?) {
        self.view.hideToastActivity()
        self.view.makeToast(message: "Stickers are unavailable for the selected country")
    }
    //MARK:- NO NETWORK
    func networkUnavailable(){
        self.view.hideToastActivity()
        self.view.makeToast(message: "Can't connect. Please check your internet connection")
    }
    //MARK:- NEXT BUTTON CLICKED
    @IBAction func nextButtonPressed(_ sender: Any) {
        // check network connectivity
        if reachability.isNetworkAvailable{
            if selectedCountry != nil {
                //API Post
                self.sendCountryName(country: selectedCountry)
                self.view.makeToastActivity(message: "Fetching..")
            }else{
                self.view.makeToast(message: "Please choose a country to continue..")
            }
        }else{
            self.networkUnavailable()
        }
    }
}
