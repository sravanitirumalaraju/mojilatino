//
//  HalfScreenCameraViewController.swift
//  Mojilatino
//
//  Created by apple on 24/08/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import SwiftyCam
import AVKit
import AVFoundation
import AssetsLibrary


//var selectedImage = UIImage()
//var videoSelected: URL?
class HomeScreenCameraViewController: UIAppViewController, SwiftyCamViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var controller = SwiftyCamViewController()
    var currentCameraType:String!
//    var recordedVideoType:String!
    
    @IBOutlet var flipCameraOutlet: UIButton!
    @IBOutlet weak var flipCameraButton: UIImageView!
    @IBOutlet var cameraContainerView: UIView!
    @IBOutlet var cameraImageButton: SwiftyRecordButton!
    @IBOutlet var galleryImage: UIImageView!
    @IBOutlet var flashOutlet: UIButton!
    @IBOutlet weak var flashButtonImage: UIImageView!
    @IBOutlet var overlayView: UIView!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        currentCameraType = "rear"
        cameraImageButton.delegate = self.controller
        addSwiftyCamToContainerView()
        controller.cameraDelegate = self.controller as? SwiftyCamViewControllerDelegate
        
    }
    //MARK:- ADDING SWIFTY CAMERA TO VIEW
    func addSwiftyCamToContainerView(){
        controller.cameraDelegate = self
//        controller.maximumVideoDuration = 10.0
//        controller.videoQuality = .resolution1920x1080
        controller.shouldUseDeviceOrientation = true
        controller.lowLightBoost =  true
        controller.allowBackgroundAudio = true
        controller.doubleTapCameraSwitch = false
        addChildViewController(controller)
        
        self.cameraContainerView.addSubview(controller.view)
        cameraContainerView.bringSubview(toFront: overlayView)
        
        controller.view.frame = view.bounds
        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        addSwiftyCamToContainerView()
    }
    //MARK:- VIEW DID APPEAR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addSwiftyCamToContainerView()
       
    }
    //MARK:-hide status bar
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    //MARK:- Swifty cam delegate functions
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController: SquareImageChangeCountry = storyboard.instantiateViewController(withIdentifier :"squareImageChangeCountry") as! SquareImageChangeCountry
        // calculate width of image in pixels
        let widthInPixels = photo.size.width * photo.scale
        
        // crop the image to get 1 : 1 square image
        viewController.passedCropImage = photo.crop(rect: CGRect(x: 0, y: 0, width: widthInPixels, height: widthInPixels))
        self.present(viewController, animated: true, completion: nil)
        
    }
    
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
//        print("Did Begin Recording")
//        UIView.animate(withDuration: 0.25, animations:{
//            self.cameraImageButton.growButton()
//            self.flashButtonImage.alpha = 0.0
//            self.flipCameraButton.alpha = 0.0
//        })
//    }
    
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
//        // Called when stopVideoRecording() is called
//        // Called if a SwiftyCamButton ends a long press gesture
//        UIView.animate(withDuration: 0.25, animations: {
//            self.cameraImageButton.shrinkButton()
//            self.flashButtonImage.alpha = 1.0
//            self.flipCameraButton.alpha = 1.0
//        })
//        switch currentCameraType{
//        case "front":
//            recordedVideoType =  VideoTypeFromRecording.SQUARE_FRONT.rawValue
//            break
//        case "rear":
//            recordedVideoType =  VideoTypeFromRecording.SQUARE_REAR.rawValue
//            break
//        default:
//            break
//        }
//    }
    
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
//        if let videoUrl:URL = url{
//            let viewController : ChooseCountryForVideoController = storyboard!.instantiateViewController(withIdentifier :"chooseCountryForVideoController") as! ChooseCountryForVideoController
//            viewController.videoUrlpassed =  url
//            viewController.cameraType = currentCameraType
//            viewController.recordedVideoType = self.recordedVideoType
//            self.present(viewController, animated: true, completion: nil)
//
//        }
//    }
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
//        print(error)
//        self.view.makeToast(message: "Video recording is failed. Please try again")
//    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }, completion: { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }, completion: { (success) in
                focusView.removeFromSuperview()
            })
        })
        
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        // Called when a user initiates a pinch gesture on the preview layer
        // Will only be called if pinchToZoomn = true
        // Returns a CGFloat of the current zoom level
    }
    
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
//        // Called when user switches between cameras
//        // Returns current camera selection
//        currentCameraType =  String(describing: camera)
//        print(currentCameraType)
//
//        switch currentCameraType{
//        case "front":
//            controller.videoQuality = .resolution640x480
//            break
//        case "rear":
//            controller.videoQuality = .resolution1920x1080
//            break
//        default:
//            break
//        }
//    }
    
    //MARK:- FULL SCREEN CAMERA CLICKED
    @IBAction func fullScreenCameraButtonClicked(_ sender: Any) {
        // performSegue(withIdentifier: "segueTofullScreenViewController", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : FullScreenController = storyboard.instantiateViewController(withIdentifier :"fullScreenController") as! FullScreenController
        self.present(viewController, animated : false, completion: nil)
    }
    
    //MARK:- picks image from gallery
    @IBAction func pickImageFromGallery(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.mediaTypes = ["public.image", "public.movie"]
        self.present(imagePicker, animated : false, completion: nil)
    }
    
    //MARK:- Create a thumbnail image
    func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    //MARK:- image picked from gallery
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            dismiss(animated:true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : SquareImageChangeCountry = storyboard.instantiateViewController(withIdentifier :"squareImageChangeCountry") as! SquareImageChangeCountry
            let widthInPixels = image.size.width * image.scale
            
            viewController.passedCropImage = image.crop(rect: CGRect(x: 0, y: 0, width: widthInPixels, height: widthInPixels))
            print(viewController.passedCropImage!)
            print(widthInPixels)
            self.present(viewController, animated: true, completion: nil)
        }
//        else if let video = info[UIImagePickerControllerReferenceURL] as? URL{
//            dismiss(animated:true, completion: nil)
//            if let videoUrl:URL = video{
//                let viewController : ChooseCountryForVideoController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier :"chooseCountryForVideoController") as! ChooseCountryForVideoController
//                viewController.videoUrlpassed =  videoUrl
//                viewController.isVideoTakenFromGallery =  true
//
//                self.present(viewController, animated: true, completion: nil)
//            }
//        }
        else{
            print("Error...")
        }
    }
    //MARK:- FLIP CAMERA CLICKED
    @IBAction func flipCameraButtonPressed(_ sender: Any) {
        print("button pressed")
        //controller.videoQuality = .resolution1920x1080
        controller.switchCamera()
    }
    @IBAction func logoutButtonPressed(_ sender: Any) {
        print("button pressed")
        self.removeTokenFromKeyChainWrapper()
    }
    //MARK:- FLASH BUTTON CLICKED
    @IBAction func flashButtonClicked(_ sender: Any) {
        controller.flashEnabled = !controller.flashEnabled
        if controller.flashEnabled == true {
            // flashOutlet.setImage(#imageLiteral(resourceName: "flash"), for: UIControlState())
            flashButtonImage.image =  UIImage(named: "flash")
        } else {
            //  flashOutlet.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
            flashButtonImage.image =  UIImage(named: "flashOutline")
        }
    }
}
//MARK:- EXTENSION IMAGE
extension UIImage {
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
    
}


