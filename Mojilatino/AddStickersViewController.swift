//
//  AddStickersViewController.swift
//  Mojilatino
//
//  Created by apple on 28/08/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit
import AssetsLibrary
import Alamofire

var newDrag: UIImage?

class AddStickersViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    var selected: UIImageView?
//    var interstitial: GADInterstitial!
    var selectedSegmentIndex:Bool =  true
    var videoImageUrlFromChangeCountryController : URL?
    var passedImage2: UIImage?
    var searchArray = [String]()
    var selectedType: String?
    
    @IBOutlet var swipeOutlet: UIView!
    @IBOutlet var segmentedStickersOutlet: UISegmentedControl!
    @IBOutlet var textFieldStickers: UITextField!
    @IBOutlet var saveOutlet: UIButton!
    @IBOutlet var hideView: UIView!
    @IBOutlet var heightUIView: NSLayoutConstraint!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var imageClicked: UIImageView!
    @IBOutlet var viewUp: UIView!
    @IBOutlet weak var canvasView: UIView!
    
    var indexTag = 0
    let trashButton = UIButton()
//    let BorderView = CAShapeLayer()
    var dragView = [DraggableStickerView]()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedType = "Normal"  // Initial setup
        
        addInitialUIChanges()
        addGestureRecognizers()
//        interstitial = createAndLoadInterstitial()
        segmentedStickersOutlet.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        self.segmentedStickersOutlet.addTarget(self, action: #selector(self.segmentedControlForStickers(_ :)), for: .valueChanged)
        if passedImage2 != nil{
            print(passedImage2!)
            imageClicked.image = passedImage2
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imageClicked.addGestureRecognizer(tap)
        imageClicked.isUserInteractionEnabled = true
        
//        BorderView.strokeColor = UIColor.red.cgColor
//        BorderView.lineDashPattern = [12, 5]
//        BorderView.lineWidth = 2
//        BorderView.fillColor = UIColor.clear.cgColor
//        BorderView.strokeStart = 0
//        BorderView.strokeEnd = 1
    }
    //MARK:- function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        //       BorderView.removeFromSuperlayer()
//        BorderView.isHidden = true
        trashButton.isHidden = true
    }
    //MARK:- ADD INITIAL CHANGES
    func addInitialUIChanges(){
        saveOutlet.layer.cornerRadius = 15.0
        saveOutlet.layer.borderColor = UIColor.black.cgColor
        saveOutlet.layer.borderWidth = 1.0
        self.textFieldStickers.layer.borderColor = UIColor.black.cgColor
        self.textFieldStickers.layer.borderWidth = 1.0
        self.textFieldStickers.layer.cornerRadius = 15
        swipeOutlet.isHidden = true
    }
    //MARK:- ADD GESTURE RECOGNIZERS
    func addGestureRecognizers(){
        addSwipeGesture()
        addTapGesture()
    }
//    func createAndLoadInterstitial() -> GADInterstitial {
//        let interstitial = GADInterstitial(adUnitID: adMobUnitID)
//        // let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
//        interstitial.delegate = self
//        interstitial.load(GADRequest())
//        return interstitial
//    }
//
//    //MARK:- AdMob delegates
//
//    /// Tells the delegate an ad request succeeded.
//    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//        print("interstitialDidReceiveAd")
//    }
//
//    /// Tells the delegate an ad request failed.
//    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
//        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
//    }
//
//    /// Tells the delegate that an interstitial will be presented.
//    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
//        print("interstitialWillPresentScreen")
//    }
//
//    /// Tells the delegate the interstitial is to be animated off the screen.
//    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialWillDismissScreen")
//    }
//
//    /// Tells the delegate the interstitial had been animated off the screen.
//    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialDidDismissScreen")
//        interstitial = createAndLoadInterstitial()
//        if videoImageUrlFromChangeCountryController != nil{
//            saveVideo()
//        } else{
//
//            UIImageWriteToSavedPhotosAlbum(canvasView.toImage(),self, #selector(AddStickersViewController.image(_:withPotentialError:contextInfo:)), nil)
//        }
//    }
//
//    /// Tells the delegate that a user click will open another app
//    /// (such as the App Store), backgrounding the current app.
//    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
//        print("interstitialWillLeaveApplication")
//    }
    //MARK:- DOWNLOAD IMAGE FROM WEB
    func downloadImageFromWeb(s : String, imageView: UIImageView){
        let imageUrl = URL(string: s)
        imageView.kf.setImage(with: imageUrl)
    }
    //MARK:- ADD SWIPE GESTURE
    func addSwipeGesture(){
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
        self.swipeOutlet.addGestureRecognizer(swipeGestureRecognizer)
    }
    //MARK:- ADD TAP GESTURE
    func addTapGesture(){
        textFieldStickers.isHidden = false
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(tapAction(tap:)))
        //  tapGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
        self.swipeOutlet.addGestureRecognizer(tapGestureRecognizer)
    }
    func swipeAction(swipe: UISwipeGestureRecognizer){
        print("swipe happened")
        self.heightUIView.constant = 0 + self.viewUp.frame.size.height
        textFieldStickers.isHidden = false
        swipeOutlet.isHidden = true
    }
    func tapAction(tap: UISwipeGestureRecognizer){
        print("tap happened")
        self.heightUIView.constant = 0 + self.viewUp.frame.size.height
        textFieldStickers.isHidden = false
        swipeOutlet.isHidden = true
        
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        addInitialUIChanges()
        addGestureRecognizers()
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    //MARK:-textfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder
        UIView.animate(withDuration: 0.50, animations: {
            self.heightUIView.constant = 250
            //    self.viewUp.layoutIfNeeded()
        }, completion: nil)
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.50, animations: {
            //Set x position what ever you want
            textField.resignFirstResponder()
            self.heightUIView.constant = 0
            //   self.viewUp.layoutIfNeeded()
        }, completion: nil)
    }
    
    //MARK:-collection view delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch selectedType {
        case "Normal"?:
            if imagesArrayNormal.count == 0{
                textFieldStickers.placeholder = "No stickers Found"
            }else{
                textFieldStickers.placeholder = "Stickers"
            }
            return imagesArrayNormal.count
        case "Business"?:
            print(imagesArrayBussiness)
            if imagesArrayBussiness.count == 0{
                textFieldStickers.placeholder = "No stickers Found"
            }else{
                textFieldStickers.placeholder = "Stickers"
            }
            return imagesArrayBussiness.count
        case "Search"?:
             print("StickersSearchImplemented")
            return searchArray.count
        default:
            return imagesArrayNormal.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! MyCollectionViewCell
        switch selectedType {
        case "Normal"?:
            downloadImageFromWeb(s: imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("normal stickers loaded")
        case "Business"?:
            downloadImageFromWeb(s: imagesArrayBussiness[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("business stickers loaded")
        case "Search"?:
            downloadImageFromWeb(s: searchArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("search stickers loaded")
        default:
            self.downloadImageFromWeb(s: (imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: cell.myImageView)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: self.view.frame.size.width/5, height: self.view.frame.size.width/5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        textFieldStickers.isHidden = true
        _ = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! MyCollectionViewCell
        indexTag = indexPath.row
        stickerAdded()
    }
    
    //MARK:- sticker added to image
    func stickerAdded(){
        UIView.animate(withDuration: 0.50, animations: {
            self.swipeOutlet.isHidden = false
            self.heightUIView.constant = (self.heightUIView.constant) - (self.viewUp.frame.size.height)
            self.textFieldStickers.resignFirstResponder()
        }, completion: nil)
        
        let draggableview = DraggableStickerView()
        draggableview.frame =  CGRect(x: 100, y: 100, width: 120, height: 150)
        draggableview.isUserInteractionEnabled = true
        draggableview.backgroundColor = UIColor.clear
        dragView.append(draggableview)
        self.imageClicked.addSubview(draggableview)
        
        let selectedSticker = UIImageView()
        selectedSticker.frame =  CGRect(x: 0, y: 30, width: 120, height: 120)
        selectedSticker.isUserInteractionEnabled = true
        selectedSticker.isMultipleTouchEnabled = true
        draggableview.addSubview(selectedSticker)
        newDrag = selectedSticker.toImage()
        selectedSticker.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(stickerTapped(_:))))
        
//        BorderView.isHidden = false
//        BorderView.frame = selectedSticker.bounds
//        BorderView.path = UIBezierPath(rect: BorderView.bounds).cgPath
//        selectedSticker.layer.addSublayer(BorderView)
        
        trashButton.isHidden = false
        trashButton.frame  = CGRect(x: -15, y: -15, width: 40, height: 40)
        trashButton.setImage(UIImage(named :"deleteSticker"), for: .normal)
//         trashButton.setImage(UIImage(named :"cancelImage0"), for: .normal)
        trashButton.isUserInteractionEnabled = false
        selectedSticker.addSubview(trashButton)
        
        let cancelBtn = UIButton()
        cancelBtn.frame = CGRect(x: -20, y: -20, width: 50, height: 50)
        cancelBtn.tag = dragView.endIndex - 1
        cancelBtn.setImage(UIImage(named :""), for: .normal)
        cancelBtn.isUserInteractionEnabled = true
        cancelBtn.addTarget(self, action: #selector(removeSticker(_:)), for: .touchUpInside)
        selectedSticker.addSubview(cancelBtn)
        
        switch selectedType {
        case "Normal"?:
            downloadImageFromWeb(s: imagesArrayNormal[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: selectedSticker)
        case "Business"?:
            downloadImageFromWeb(s: imagesArrayBussiness[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: selectedSticker)
        case "Search"?:
            self.downloadImageFromWeb(s: (searchArray[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: selectedSticker)
        default:
            self.downloadImageFromWeb(s: (imagesArrayNormal[indexTag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: selectedSticker)
        }
    }
    //MARK:- remove sticker tapped
    func removeSticker(_ sender: UIButton) {
        print("action delete")
        dragViewTapped(dragView[sender.tag])
    }
    func dragViewTapped(_ sender: DraggableStickerView){
        sender.removeFromSuperview()
    }
    //MARK:- Sticker tapped
    func stickerTapped(_ sender: UITapGestureRecognizer){
        let tappedImageView = sender.view!
//        if BorderView.isHidden == false{
//            BorderView.isHidden = true
//            trashButton.isHidden = true
//        }else{
//            BorderView.isHidden = false
            trashButton.isHidden = false
            
//            BorderView.frame = tappedImageView.bounds
//            BorderView.path = UIBezierPath(rect: BorderView.bounds).cgPath
//            tappedImageView.layer.addSublayer(BorderView)
        
            trashButton.frame  = CGRect(x: -15, y: -15, width: 40, height: 40)
            tappedImageView.addSubview(trashButton)
//        }
    }
    func segmentedControlForStickers(_ sender: UISegmentedControl){
        switch segmentedStickersOutlet.selectedSegmentIndex{
        case 0:
            selectedType = "Normal"
            self.collectionView.reloadData()
        case 1:
            selectedType = "Business"
            self.collectionView.reloadData()
        default:
            selectedType = "Normal"
            self.collectionView.reloadData()
        }
    }
    //MARK:- SAVE VIDEO
    func saveVideo(){
        // Use Merge.swift file to merge stickers with video
        let merge = Merge(config: .standard)
        //convert url into AVAsset object
        let selectedVideoObject = AVAsset(url: videoImageUrlFromChangeCountryController!)
        print("selectedVideoObject : \(selectedVideoObject) +  newDrag :\(String(describing: newDrag))")
        merge.overlayVideo(video: selectedVideoObject, overlayImage: newDrag! , completion: { url in
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url!)
            }) { saved, error in
                if saved {
                    let alertController = UIAlertController(title: "Your video was successfully saved", message: nil, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
                        self.present(viewController, animated: true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }){ progress in
        }
    }
    
    func image(_ image: UIImage, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer){
        let alert = UIAlertController(title: "Image Saved", message: "Image successfully saved to Photos library", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
            self.present(viewController, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func requestError(error: Error?) {
        self.view.hideToastActivity()
        self.view.makeToast(message: (error?.localizedDescription)!)
    }
    //MARK:- NEXT BUTTON CLICKED
    @IBAction func nextButtonPressed(_ sender: Any) {
        viewUp.isHidden = true
        hideView.isHidden = true
        swipeOutlet.isHidden = true
        if passedImage2 != nil{
//            if interstitial.isReady {
//                interstitial.present(fromRootViewController: self)
//            } else {
                print("Ad wasn't ready")
                if videoImageUrlFromChangeCountryController != nil{
                    saveVideo()
                } else{
//                    BorderView.strokeColor = UIColor.clear.cgColor
//                    BorderView.removeFromSuperlayer()
                    trashButton.removeFromSuperview()
                    //If there are no adds to show, then save the image
                    UIImageWriteToSavedPhotosAlbum(canvasView.toImage(),self, #selector(AddStickersViewController.image(_:withPotentialError:contextInfo:)), nil)
                }
//            }
        }
    }
    //MARK:- BACK BUTTON CLICKED
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- TEXT FILED DELEGATES
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        print("action triggered")
        if segmentedStickersOutlet.selectedSegmentIndex == 0{
            searchArray.removeAll()
            for (_, element) in normalStickerArray.enumerated(){
                if (element["tag"] as! String).hasPrefix(textFieldStickers.text!){
                    searchArray.append(element["imgurl"] as! String)
                }
            }
        } else if segmentedStickersOutlet.selectedSegmentIndex == 1{
            searchArray.removeAll()
            for (_, element) in businessStickerArray.enumerated(){
                if (element["tag"] as! String).hasPrefix(textFieldStickers.text!){
                    searchArray.append(element["imgurl"] as! String)
                }
            }
        }
        selectedType = "Search"
        collectionView.reloadData()
    }
}

//MARK:- Merge extension
extension MergeConfiguration{
    static var standard: MergeConfiguration {
        return MergeConfiguration(frameRate: 30, directory: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0], quality: Quality.high, placement: Placement.stretchFit)
    }
}
//MARK:- Extentions for UIImage
extension UIImage{
    func mergeImages(imageView: UIImageView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(imageView.frame.size, false, 0.0)
        imageView.superview!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

//MARK:-combining two images
extension UIImage {
    func combineWith(image: UIImage) -> UIImage {
        let size = CGSize(width: self.size.width, height: self.size.height + image.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        self.draw(in: CGRect(x:0 , y: 0, width: size.width, height: self.size.height))
        image.draw(in: CGRect(x: 0, y: self.size.height, width: size.width,  height: image.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
//MARK:- creating a transparent image
extension UIImage {
    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
