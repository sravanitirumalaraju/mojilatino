//
//  SquareImageChangeCountry.swift
//  Mojilatino
//
//  Created by apple on 23/10/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import SwiftyCam

class SquareImageChangeCountry: UIAppViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    var imageFromPreviousViewController : UIImage?
    var videoUrlpassed: URL?
    var thumbnailVideoImage: UIImage?
    //    var selectedState: Bool? = nil
    var selectedIndex: Int?
    var selectedCountry: String?
    var passedCropImage: UIImage?
    //var listOfCountries = ["India", "Australia", "Sri Lanka", "Kenya", "Afghanistan"]
    
    @IBOutlet var imageViewimage: UIImageView!
    @IBOutlet var changeCountry: UILabel!
    @IBOutlet var dropDown: UIPickerView!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var buttonLabel: UIButton!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        addInitialChanges()
        requestManager.delegate = self
        if passedCropImage != nil{
            imageViewimage.contentMode = .scaleAspectFill
            imageViewimage.clipsToBounds = true
            //    imageViewimage.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            imageViewimage.image = passedCropImage
        }
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        addInitialChanges()
    }
    //MARK:- ADD INITIAL CHANGES
    func addInitialChanges(){
        self.nextButton.addBlackShadeWithRoundedEdges()
        changeCountry.layer.cornerRadius = 15.0
        changeCountry.clipsToBounds = true
        buttonLabel.setTitleColor(UIColor.white, for: .normal)
    }
    //MARK:- SEND COUNTRY NAME
    func sendCountryName(country : String?){
        self.requestManager.sendCountryData(country: country!)
    }
    
    //MARK:- Picker delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listOfCountries.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listOfCountries[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountry = listOfCountries[row]
        self.changeCountry.text = listOfCountries[row]
        selectedIndex = row
        self.dropDown.isHidden = true
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = listOfCountries[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.white])
        return myTitle
    }
    func image(_ image: UIImage, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        let alert = UIAlertController(title: "Image Saved", message: "Image successfully saved to Photos library", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
            self.present(viewController, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- NEXT BUTTON
    @IBAction func next(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(passedCropImage!,self, #selector(SquareImageChangeCountry.image(_:withPotentialError:contextInfo:)), nil)
    }
    //MARK:- SHOW STICKERS
    func showStickers(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.view.hideToastActivity()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController: AddStickersForSquareImage = storyboard.instantiateViewController(withIdentifier :"addStickersForSquareImage") as! AddStickersForSquareImage
            viewController.videoImageUrlFromChangeCountryController = self.videoUrlpassed
            viewController.passedImage2 = self.imageViewimage.image
            self.present(viewController, animated: true)
        }
    }
    func requestError(error: Error?) {
        self.view.hideToastActivity()
        self.view.makeToast(message: "Stickers are unavailable for the selected country")
        // self.view.makeToast(message: (error?.localizedDescription)!)
    }
    //MARK:- NETWORK NOT AVAILABLE
    func networkUnavailable(){
        self.view.hideToastActivity()
        self.view.makeToast(message: "Can't connect. Please check your internet connection")
    }
    //MARK:- BUTTON PRESSED
    @IBAction func buttonPressed(_ sender: Any) {
        dropDown.isHidden = false
    }
    
    @IBAction func dismissViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- NEXT BUTTON CLICKED
    @IBAction func nextButtonPressed(_ sender: Any) {
        if reachability.isNetworkAvailable{
            if selectedCountry != nil {
                //API Post
                self.sendCountryName(country: selectedCountry)
                self.view.makeToastActivity(message: "Fetching..")
            }else{
                self.view.makeToast(message: "Please choose a country to continue..")
            }
        }else{
            self.networkUnavailable()
        }
    }
}
