//
//  EZButton.swift
//  EyaalZayeed
//
//  Created by apple on 16/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation
import UIKit

class EZButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeRoundedEdge(boarderWidth: 1.0, borderColour: UIColor.white, cornerRadius: 15.0)
        self.clipsToBounds = true
        makeShadowButton()
        
    }
    
}


extension UIButton{
    
    func makeRoundedEdge(boarderWidth:CGFloat?, borderColour: UIColor?,cornerRadius:CGFloat?)
    {
        layer.cornerRadius = cornerRadius!
        layer.borderWidth = boarderWidth!
        layer.borderColor = borderColour?.cgColor
    }
    
    func makeShadowButton(){
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        layer.masksToBounds = false
        
        
    }
}


