//
//  AppDelegate.swift
//  Mojilatino
//
//  Created by apple on 20/07/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
//import Firebase

let serverEndPoint = "http://www.mojilatino.com"
let adMobAppId = "ca-app-pub-6010456102017397~7693415828"
let adMobUnitID = "ca-app-pub-6010456102017397/8808376317"

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      // FIRApp.configure()
        Thread.sleep(forTimeInterval: 2)
//        GADMobileAds.configure(withApplicationID: adMobAppId)
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
       // FBSDKAppEvents.activateApp()
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
       // AppEventsLogger.activate(application)
    }
    func applicationWillTerminate(_ application: UIApplication) {
    }
}
