//
//  SignUpTableViewCell2.swift
//  Mojilatino
//
//  Created by apple on 21/07/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit

class SignUpTableViewCell2: UITableViewCell {
    
    @IBOutlet var facebookButtonOutlet: UIButton!
    @IBOutlet var googleButtonOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
