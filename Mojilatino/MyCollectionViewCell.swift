//
//  MyCollectionViewCell.swift
//  Mojilatino
//
//  Created by apple on 28/08/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var myImageView: UIImageView!
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func awakeFromNib() {
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge, color: .red,  placeInTheCenterOf: self.contentView)
    }
    
}
