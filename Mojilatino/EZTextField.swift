//
//  EZTextField.swift
//  EyaalZayeed
//
//  Created by apple on 14/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit
import Foundation

@IBDesignable
class EZTextField: UITextField, UITextFieldDelegate {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        makeRoundedEdge(boarderWidth: 1.0, borderColour: UIColor.white, cornerRadius: 22.0)
        
         attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSForegroundColorAttributeName: UIColor.white])
        

    }
    
    
}


extension UITextField{
    func makeRoundedEdge(boarderWidth:CGFloat?, borderColour: UIColor?,cornerRadius:CGFloat?)
    {
        layer.cornerRadius = cornerRadius!
        layer.borderWidth = boarderWidth!
        layer.borderColor = borderColour?.cgColor
        
    }
}

    

