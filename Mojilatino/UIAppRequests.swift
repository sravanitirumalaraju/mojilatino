//
//  UIAppRequests.swift
//  Mojilatino
//
//  Created by apple on 09/10/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper

var normalStickerArray = [[String : Any]]()
var businessStickerArray = [[String : Any]]()
var imagesArrayNormal = [String]()
var imagesArrayBussiness = [String]()
var searchNormalDictionary = [String : Any]()

class UIAppServerRequests: NSObject {
    
    var delegate:RequestManagerDelegate? = nil
    
    final  class var sharedInstance : UIAppServerRequests {
        struct Static {
            static var instance : UIAppServerRequests?
        }
        if !(Static.instance != nil) {
            Static.instance = UIAppServerRequests()
            
        }
        return Static.instance!
    }
    
    func sendCountryData(country:String) {
        print("test api")
        imagesArrayNormal.removeAll()
        imagesArrayBussiness.removeAll()
        normalStickerArray.removeAll()
        businessStickerArray.removeAll()
        let headers: HTTPHeaders = ["Content-Type": "application/json"]
        let endPoint1 = serverEndPoint + "/GetStickersByCountry/\(country)"
        let safeURL = endPoint1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let _:Dictionary = ["country" : "\(country)"]
        Alamofire.request(safeURL, headers: headers).responseJSON { response in
            switch(response.result) {
            case .success(let value):
                let json = JSON(value)
                print("json  :\(json)")
                for (_, value) in json{
                    if let stickerCategory = value["country"]["category"].string {
                        if stickerCategory != "" && stickerCategory == "Normal"{
                            normalStickerArray.append(value["country"].dictionaryObject!)
                            let imageurl = (value["country"]["imgurl"].string!).removingPercentEncoding
                            print("imageurl @ api response : \(String(describing: imageurl))")
                            imagesArrayNormal.append(imageurl!)
                        } else if stickerCategory != "" && stickerCategory == "Business"{
                            businessStickerArray.append(value["country"].dictionaryObject!)
                            let imageurl = (value["country"]["imgurl"].string!).removingPercentEncoding
                            print("imageurl @ api response-biz : \(String(describing: imageurl))")
                            imagesArrayBussiness.append(imageurl!)
                        }
                    }
                }
                if response.response?.statusCode == 200{
                    self.delegate?.showStickers!()
                }
            case .failure(_):
                print(response.result.error!)
                self.delegate?.requestError!(error: response.result.error)
                break
            }
            debugPrint(response)
        }
    }
}
