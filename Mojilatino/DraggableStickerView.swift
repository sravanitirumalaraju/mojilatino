//
//  DraggableStickerView.swift
//  Mojilatino
//
//  Created by apple on 30/08/17.
//  Copyright © 2017 havells. All rights reserved.
//

import Foundation
import UIKit

class DraggableStickerView: UIView, UIGestureRecognizerDelegate {
    
    var dragStartPositionRelativeToCenter : CGPoint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
//        addSwipeGestureRecognizer()
        addDraggableBehavior()
        addPinchToZoomBehaviour()
        addRotationGestureRecognizer()
        
//        addArrows()
    }
    func addArrows(){
        let leftArrow = UIButton.init(frame: CGRect(x: -15, y: 75, width: 30, height: 30))
        leftArrow.setImage(#imageLiteral(resourceName: "left"), for: .normal)
        leftArrow.backgroundColor = UIColor.black
        leftArrow.addTarget(self, action: #selector(addSwipeGestureRecognizer), for: .touchUpInside)
        self.addSubview(leftArrow)
        
        let belowArrow = UIButton.init(frame: CGRect(x: (self.frame.size.width / 2) - 30, y: self.frame.size.height, width: 30, height: 30))
        belowArrow.setImage(#imageLiteral(resourceName: "Down"), for: .normal)
        belowArrow.backgroundColor = UIColor.red
        self.addSubview(belowArrow)
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    func addDraggableBehavior() {
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:))))
    }
    
    func addPinchToZoomBehaviour(){
        addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(self.handlePinch(_:))))
    }
    //sravani
    func addLongPressGestureRecognizer(){
        addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longPressOnSticker(press:))))
    }
    func addSwipeGestureRecognizer(){
        addGestureRecognizer(UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:))))
    }
    func handleSwipe(_ sender: UISwipeGestureRecognizer){
        if sender.direction == .left{
            print("left")
        }else if sender.direction == .down{
            print("down")
        }else if sender.direction == .right{
            print("right")
        }else{
            print("up")
        }
    }
    func addRotationGestureRecognizer(){
        addGestureRecognizer(UIRotationGestureRecognizer(target: self, action: #selector(self.handleRotation(_sender:))))
    }
    func handleRotation(_sender: UIRotationGestureRecognizer){
        print("rotate")
        UIView.animate(withDuration: 0.1, animations: {
            let rotatePoint = _sender.location(in: self)
            let ourEmojiView = self.hitTest(rotatePoint, with: nil)
            ourEmojiView?.transform = (ourEmojiView?.transform.rotated(by:_sender.rotation))!
            _sender.rotation = 0
        })
    }
    
    func handlePan(_ sender: UIPanGestureRecognizer!) {
        if sender.state == UIGestureRecognizerState.began {
            let locationInView = sender.location(in: superview)
            dragStartPositionRelativeToCenter = CGPoint(x: locationInView.x - center.x, y: locationInView.y - center.y)
            layer.shadowOffset = CGSize(width: 0, height: 20)
            layer.shadowOpacity = 0.3
            layer.shadowRadius = 6
//            layer.backgroundColor = UIColor.green.cgColor
            return
        }
        if sender.state == UIGestureRecognizerState.ended {
            dragStartPositionRelativeToCenter = nil
            
            layer.shadowOffset = CGSize(width: 0, height: 3)
            layer.shadowOpacity = 0.5
            layer.shadowRadius = 2
            return
        }
        let locationInView = sender.location(in: superview)
        
        UIView.animate(withDuration: 0.1) {
            self.center = CGPoint(x: locationInView.x - self.dragStartPositionRelativeToCenter!.x,
                                  y: locationInView.y - self.dragStartPositionRelativeToCenter!.y)
        }
    }
    // handle UIPinchGestureRecognizer
    func handlePinch(_ sender: UIPinchGestureRecognizer) {
        print("pinch begins")
        stopWiggle(for: self)
        print(sender.state == .recognized)
        if sender.state == .began || sender.state == .changed{
            sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
            sender.scale = 1.0
        }
    }
    func longPressOnSticker(press: UILongPressGestureRecognizer!){
        press.minimumPressDuration = 0.5
        if press.state == .began{
            print("long press")
            startWiggle(for: self)
        }
    }
    
    //Wiggle animation to delete sticker
    let wiggleBounceY = 4.0
    let wiggleBounceDuration = 0.12
    let wiggleBounceDurationVariance = 0.025
    let wiggleRotateAngle = 0.06
    let wiggleRotateDuration = 0.10
    let wiggleRotateDurationVariance = 0.025
    func randomize(interval: TimeInterval, withVariance variance: Double) -> Double{
        let random = (Double(arc4random_uniform(1000)) - 500.0) / 500.0
        return interval + variance * random
        
    }
    func startWiggle(for view: UIView) {
//        trashButton?.isHidden = false
        //Create rotation animation
        let rotationAnim = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotationAnim.values = [-wiggleRotateAngle, wiggleRotateAngle]
        rotationAnim.autoreverses = true
        rotationAnim.duration = randomize(interval: wiggleRotateDuration, withVariance: wiggleRotateDurationVariance)
        rotationAnim.repeatCount = HUGE
        
        //Create bounce animation
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        bounceAnimation.values = [wiggleBounceY, 0]
        bounceAnimation.autoreverses = true
        bounceAnimation.duration = randomize(interval: wiggleBounceDuration, withVariance: wiggleRotateDurationVariance)
        bounceAnimation.repeatCount = HUGE
        
        //Apply animations to view
        UIView.animate(withDuration: 0) {
            view.layer.add(rotationAnim, forKey: "rotation")
            view.layer.add(bounceAnimation, forKey: "bounce")
            view.transform = .identity
        }
    }
    func stopWiggle(for view: UIView){
        view.layer.removeAllAnimations()
    }
    
}
