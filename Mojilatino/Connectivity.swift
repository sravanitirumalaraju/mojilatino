//
//  Connectivity.swift
//  EyaalZayeed
//
//  Created by apple on 14/08/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity:NSObject{
    
    final  class var sharedInstance : Connectivity {
        struct Static {
            static var instance : Connectivity?
        }
        if !(Static.instance != nil) {
            Static.instance = Connectivity()
            
        }
        return Static.instance!
    }
    
    public var isNetworkAvailable:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class var isConnectedToNetwork:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }

}


