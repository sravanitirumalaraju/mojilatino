//
//  AppEnum.swift
//  Mojilatino
//
//  Created by apple on 22/12/17.
//  Copyright © 2017 havells. All rights reserved.
//

import Foundation

enum VideoTypeFromRecording:String{
    
    case SQUARE_REAR = "square_rear"
    case SQUARE_FRONT = "square_front"
    case PORTRAIT_REAR = "portrait_rear"
    case PORTRAIT_FRONT = "portrait_front"
}
enum VideoTypeFromGallery:String{
    case PORTRAIT = "portrait"
    case SQUARE = "square"
}
