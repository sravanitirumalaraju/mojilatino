//
//  FullScreenController.swift
//  Mojilatino
//
//  Created by apple on 01/09/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import SwiftyCam
import AVKit
import AVFoundation
import AssetsLibrary

class FullScreenController: UIAppViewController, SwiftyCamViewControllerDelegate {
    
    @IBOutlet weak var captureButton: SwiftyRecordButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var flashButtonImage: UIImageView!
    @IBOutlet var cameraContainerView: UIView!
    @IBOutlet var cameraOverlayMenuView: UIView!
    
    var swiftyCamVC = SwiftyCamViewController()
//    var recordedVideoType:String!
    var currentCameraType:String!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        currentCameraType = "rear"
        initialUISetup()
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        initialUISetup()
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    //MARK:- VIEW DID APPEAR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // captureButton.delegate = self as? SwiftyCamButtonDelegate
    }
    //MARK:- INITIAL UI SETUP
    func initialUISetup(){
        self.addSwiftyCamToContainerView()
        captureButton.delegate = self.swiftyCamVC
    }
    //MARK:- ADD SWIFTY CAM VIEW
    func addSwiftyCamToContainerView(){
        swiftyCamVC.cameraDelegate = self
//        swiftyCamVC.maximumVideoDuration = 10.0
        swiftyCamVC.shouldUseDeviceOrientation = true
        swiftyCamVC.doubleTapCameraSwitch = false
        addChildViewController(swiftyCamVC)
        self.cameraContainerView.addSubview(swiftyCamVC.view)
        cameraContainerView.bringSubview(toFront: cameraOverlayMenuView)
        swiftyCamVC.view.frame = view.bounds
        swiftyCamVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    //MARK:-Swifty cam delegate functions
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        let newVC = PhotoViewController(image: photo)
        self.present(newVC, animated: true, completion: nil)
    }
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
//        print("Did Begin Recording")
//        UIView.animate(withDuration: 0.25, animations:{
//            self.captureButton.growButton()
//            self.flashButtonImage.alpha = 0.0
//            self.flipCameraButton.alpha = 0.0
//        })
//    }
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
//        print("Did finish Recording")
//        UIView.animate(withDuration: 0.25, animations: {
//            self.captureButton.shrinkButton()
//            self.flashButtonImage.alpha = 1.0
//            self.flipCameraButton.alpha = 1.0
//        })
//    }
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
//        if let videoUrl:URL = url{
//            let viewController : ChooseCountryForVideoController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier :"chooseCountryForVideoController") as! ChooseCountryForVideoController
//            switch currentCameraType{
//            case "front":
//                recordedVideoType =  VideoTypeFromRecording.PORTRAIT_FRONT.rawValue
//                break
//            case "rear":
//                recordedVideoType =  VideoTypeFromRecording.PORTRAIT_REAR.rawValue
//                break
//            default:
//                break
//            }
//            viewController.videoUrlpassed =  videoUrl
//            viewController.recordedVideoType = self.recordedVideoType
//            self.present(viewController, animated: true, completion: nil)
//        }
//    }
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }, completion: { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }, completion: { (success) in
                focusView.removeFromSuperview()
            })
        })
    }
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        print(zoom)
    }
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
//        currentCameraType =  String(describing: camera)
//        print(currentCameraType)
//        switch currentCameraType{
//        case "front":
//            swiftyCamVC.videoQuality = .resolution1280x720
//            break
//        case "rear":
//            swiftyCamVC.videoQuality = .resolution1920x1080
//            break
//        default:
//            break
//        }
//    }
//    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
//        print(error)
//    }
    //MARK:- CAMERA SWITCH TAPPED
    @IBAction func cameraSwitchTapped(_ sender: Any) {
        swiftyCamVC.switchCamera()
    }
    //MARK:- TOGGLE FLASH TAPPED
    @IBAction func toggleFlashTapped(_ sender: Any) {
        swiftyCamVC.flashEnabled = !swiftyCamVC.flashEnabled
        if swiftyCamVC.flashEnabled == true {
           // flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControlState())
            flashButtonImage.image =  UIImage(named: "flash")
            //swiftyCamVC.flashEnabled = false
        } else {
           // flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
            flashButtonImage.image =  UIImage(named: "flashOutline")
           // swiftyCamVC.flashEnabled = true
        }
    }
    //MARK:-BACK BUTTON CLICKED
    @IBAction func backButtonPressed(_ sender: Any) {
        print("button pressed")
        self.dismiss(animated: true, completion: nil)
    }
}
