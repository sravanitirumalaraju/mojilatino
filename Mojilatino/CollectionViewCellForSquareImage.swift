//
//  CollectionViewCellForSquareImage.swift
//  Mojilatino
//
//  Created by apple on 23/10/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit

class CollectionViewCellForSquareImage: UICollectionViewCell {
    
    @IBOutlet var myImageView: UIImageView!
    
}
