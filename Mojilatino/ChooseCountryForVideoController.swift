//
//  ChooseCountryForVideoController.swift
//  Mojilatino
//
//  Created by apple on 13/11/17.
//  Copyright © 2017 havells. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ChooseCountryForVideoController: UIAppViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet var videoPlayerView: UIView!
    @IBOutlet var buttonLabel: EZButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var changeCountry: UILabel!
    @IBOutlet var dropDown: UIPickerView!
    
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer?
    var avpController = AVPlayerViewController()
    var isVideoTakenFromGallery:Bool?
    var imageFromPreviousViewController : UIImage?
    var videoUrlpassed: URL?
    var thumbnailVideoImage: UIImage?
    var selectedIndex: Int?
    var selectedCountry: String?
    
    var cameraType:String?
    var recordedVideoType:String?
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        addInitialChanges()
        requestManager.delegate = self
        print("cameraType : \(cameraType)")
        print("recordedVideoType : \(recordedVideoType)")
        self.categorizeVideosAndMakeSquareVideoIfNecessary()
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        addInitialChanges()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    //MARK:- ADD INITIAL CHANGES
    func addInitialChanges(){
        nextButton.layer.borderColor = UIColor.black.cgColor
        nextButton.layer.cornerRadius = 15.0
        nextButton.layer.borderWidth = 1.0
        changeCountry.layer.cornerRadius = 15.0
        changeCountry.clipsToBounds = true
        
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.videoPlayerView.addGestureRecognizer(tapGestureRecognizer)
    }
    func getMediaDuration(url: URL!) -> Float64{
        let asset : AVURLAsset = AVURLAsset(url: url)
        let duration : CMTime = asset.duration
        return CMTimeGetSeconds(duration)
    }
    func setupVideoPlayer(videoUrl:URL?){
        if let videoUrl:URL = videoUrl {
            self.player = AVPlayer(url: videoUrl)
            self.player.externalPlaybackVideoGravity = AVLayerVideoGravityResizeAspectFill
            
            self.avpController = AVPlayerViewController()
            self.avpController.player = self.player
            
            //avpController.view.frame = view.bounds
            avpController.view.frame = self.videoPlayerView.bounds
            avpController.view.layer.masksToBounds = false
            self.videoPlayerView.layer.masksToBounds = true
            self.addChildViewController(avpController)
            self.videoPlayerView.addSubview(avpController.view)
            
            //Duration check
            let durationOfVideo = self.getMediaDuration(url: videoUrl)
            print("durationOfVideo : \(durationOfVideo)")

            if durationOfVideo < 1.0 {
                let alert = UIAlertController(title: "Low Duration!", message: "The video don't have enough duration.Please record again", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
                    self.present(viewController, animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
            }
            NotificationCenter.default.addObserver(self, selector: #selector(playerStalled),name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: player.currentItem)
        }
    }
    func getVideoUrlFromAVPlayer() -> URL? {
        let asset = self.player?.currentItem?.asset
        if asset == nil {
            return nil
        }
        if let urlAsset = asset as? AVURLAsset {
            return urlAsset.url
        }
        return nil
    }
    func playerStalled(note: NSNotification) {
        print("player stalled")
        let playerItem = note.object as! AVPlayerItem
        if let player = playerItem.value(forKey: "player") as? AVPlayer{
            player.play()
        }
    }
    func categorizeVideosAndMakeSquareVideoIfNecessary(){
        self.videoPlayerView.makeToastActivity(message: "Loading..")
        //Categorize video types
        if let videoTypeRecorded =  recordedVideoType{
            switch videoTypeRecorded {
            case VideoTypeFromRecording.SQUARE_REAR.rawValue:
                self.convertToSquareVideo()
                break
            case VideoTypeFromRecording.SQUARE_FRONT.rawValue:
                self.convertToSquareVideo()
                break
            case VideoTypeFromRecording.PORTRAIT_FRONT.rawValue:
                self.setupVideoPlayer(videoUrl: self.videoUrlpassed)
                break
            case VideoTypeFromRecording.PORTRAIT_REAR.rawValue:
                self.setupVideoPlayer(videoUrl: self.videoUrlpassed)
                break
            default:
                break
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.videoPlayerView.hideToastActivity()
        })
    }
    func sendCountryName(country : String?){
        self.requestManager.sendCountryData(country: country!)
    }
    func convertToSquareVideo(){
        //Create Layers for Video Output
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        let outputURL:URL = (self.videoUrlpassed)!
        print("outputURL : \(outputURL)")
        
        //Setting Up Video Composition
        let videoAsset = AVURLAsset(url: self.videoUrlpassed!, options: nil)
        let clipVideoTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
        
        let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(1, 30)
        
        //video  render size
        videoComposition.renderSize = CGSize(width: clipVideoTrack.naturalSize.height, height: (clipVideoTrack.naturalSize.height))
        
        //create a video instruction
        let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(180, 30))
        
        // transformer is applied to set the video in portrait otherwise it is rotated by 90 degrees
        let transformer: AVMutableVideoCompositionLayerInstruction =
            AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let t1: CGAffineTransform = CGAffineTransform(translationX: clipVideoTrack.naturalSize.height, y: 0)
        
        
        // Calculate rotation angle
        var angle:CGFloat = CGFloat()
        let deviceOrientation =  UIDevice.current.orientation
        
        let t2: CGAffineTransform = t1.rotated(by: CGFloat(Double.pi/2))
        let finalTransform: CGAffineTransform = t2
        transformer.setTransform(finalTransform, at: kCMTimeZero)
        
        instruction.layerInstructions = NSArray(object: transformer) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // 2 - set up the parent layer
        parentLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.height)
        parentLayer.masksToBounds =  true
        //parentLayer.backgroundColor =  UIColor.blue.cgColor
        videoLayer.frame = CGRect(x: 0, y: -(parentLayer.frame.origin.y), width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.height)
        videoLayer.masksToBounds =  true
        
        parentLayer.addSublayer(videoLayer)
        
        print("videoLayer.frame : \(videoLayer.frame)")
        print("parentLayer.frame : \(parentLayer.frame)")
        print("videoLayer.frame :\(videoLayer.frame) + parentLayer.frame:\(parentLayer.frame) ")
        
        // 3 - apply magic
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        
        //4. Saving it
        let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        
        let timeStamp = NSDate().timeIntervalSince1970
        let exportPath = NSTemporaryDirectory().appendingFormat("/video_\(timeStamp).mov")
        let exportURL = URL(fileURLWithPath: exportPath)
        
        print("exportURL : \(exportURL)")
        exporter?.outputURL =  exportURL
        exporter!.videoComposition = videoComposition
        exporter?.outputFileType = AVFileTypeMPEG4
        exporter?.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, videoAsset.duration)
        exporter?.timeRange = range
        print("exporter?.outputURL : \(exporter?.outputURL)")
        exporter?.exportAsynchronously(completionHandler: {() -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                if exporter?.status == .completed {
                    //  handler(outputUrl, nil)
//                    print(exporter?.status.rawValue)
//                    print("export completed")
                    self.setupVideoPlayer(videoUrl: exportURL)
                }else if exporter?.status == .failed {
//                    print("export failed : \(exporter?.error)")
                }else if exporter?.status == .cancelled{
//                    print("export cancelled")
                }else {
//                    print(exporter?.error!)
                }
            })
        })
    }
    //MARK:-picker delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listOfCountries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listOfCountries[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountry = listOfCountries[row]
        self.changeCountry.text = listOfCountries[row]
        selectedIndex = row
        self.dropDown.isHidden = true
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = listOfCountries[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.white])
        return myTitle
    }
    
    @IBAction func changeCountryButtonPressed(_ sender: Any) {
        dropDown.isHidden = false
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //RequestManagerDelegate
    func showStickers(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.view.hideToastActivity()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController: AddStickersToVideoController = storyboard.instantiateViewController(withIdentifier :"addStickersToVideoController") as!  AddStickersToVideoController
            
            if let cameratype = self.cameraType{
                viewController.cameraType =  self.cameraType!
            }else {
                viewController.cameraType =  "rear"
            }
            
            if let videoType =  self.recordedVideoType{
                viewController.recordedVideoType = videoType
                viewController.isVideoTakenFromGallery =  self.isVideoTakenFromGallery
                //Categorize video url based on video type
                switch videoType {
                case VideoTypeFromRecording.SQUARE_REAR.rawValue:
                    let url = self.getVideoUrlFromAVPlayer()
                    let viewController: AddStickerToSquareVideoController = storyboard.instantiateViewController(withIdentifier :"addStickerToSquareVideoController") as!  AddStickerToSquareVideoController
                    viewController.recordedVideoType = videoType
                    viewController.isVideoTakenFromGallery =  self.isVideoTakenFromGallery
                    viewController.videoUrlFromLocalVideo = url
                    
                    if let cameratype = self.cameraType{
                        viewController.cameraType =  self.cameraType!
                    }else {
                        viewController.cameraType =  "rear"
                    }
                    self.present(viewController, animated: true)
                    
                    break
                case VideoTypeFromRecording.SQUARE_FRONT.rawValue:
                    let url = self.getVideoUrlFromAVPlayer()
                    let viewController: AddStickerToSquareVideoController = storyboard.instantiateViewController(withIdentifier :"addStickerToSquareVideoController") as!  AddStickerToSquareVideoController
                    viewController.recordedVideoType = videoType
                    viewController.isVideoTakenFromGallery =  self.isVideoTakenFromGallery
                    viewController.videoUrlFromLocalVideo = url
                    
                    if let cameratype = self.cameraType{
                        viewController.cameraType =  self.cameraType!
                    }else {
                        viewController.cameraType =  "rear"
                    }
                    self.present(viewController, animated: true)
                    break
                case VideoTypeFromRecording.PORTRAIT_FRONT.rawValue:
                    viewController.videoUrlFromLocalVideo = self.videoUrlpassed
                    self.present(viewController, animated: true)
                    break
                case VideoTypeFromRecording.PORTRAIT_REAR.rawValue:
                    viewController.videoUrlFromLocalVideo = self.videoUrlpassed
                    self.present(viewController, animated: true)
                    break
                default:
                    break
                }
                print("videoType : \(videoType)")
            }
            //            if let videoUrl:URL = self.videoUrlpassed {
            //                viewController.videoUrlFromLocalVideo = self.videoUrlpassed
            //                viewController.isVideoTakenFromGallery =  self.isVideoTakenFromGallery
            //            }
        }
    }
    func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        self.player.play()
    }
    func requestError(error: Error?) {
        self.view.hideToastActivity()
        self.view.makeToast(message: "Stickers are unavailable for the selected country")
    }
    func networkUnavailable(){
        self.view.hideToastActivity()
        self.view.makeToast(message: "Can't connect. Please check your internet connection")
    }
    @IBAction func nextButtonPressed(_ sender: Any) {
        // check network connectivity
        if reachability.isNetworkAvailable{
            if selectedCountry != nil {
                //API Post
                self.sendCountryName(country: selectedCountry)
                self.view.makeToastActivity(message: "Fetching..")
            }else{
                self.view.makeToast(message: "Please choose a country to continue..")
            }
        }else{
            self.networkUnavailable()
        }
    }
    
}

