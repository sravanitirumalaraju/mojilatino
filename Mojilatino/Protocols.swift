//
//  Protocols.swift
//  Mojilatino
//
//  Created by apple on 09/10/17.
//  Copyright © 2017 havells. All rights reserved.
//

import Foundation
import SwiftyJSON

@objc protocol RequestManagerDelegate:NSObjectProtocol {
    @objc optional func loginSuccess()
    @objc optional func showStickers()
   
    //Common Errors
    
    @objc optional func requestError(error:Error?)
    @objc optional func networkUnavailable()
}
    


