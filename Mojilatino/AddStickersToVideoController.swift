//
//  AddStickersToVideoController.swift
//  Mojilatino
//
//  Created by apple on 13/11/17.
//  Copyright © 2017 havells. All rights reserved.
//


import UIKit
import Photos
import AVFoundation
import AVKit
import AssetsLibrary
import Alamofire
import QuartzCore

class AddStickersToVideoController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    //video player
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer?
    var avpController = AVPlayerViewController()
    
    var videoUrlFromLocalVideo: URL?
    var isVideoTakenFromGallery:Bool?
    var selected: UIImageView?
//    var interstitial: GADInterstitial!
    var selectedSegmentIndex:Bool =  true
    var videoImageUrlFromChangeCountryController : URL?
    var passedImage2: UIImage?
    var searchArray = [String]()
    var selectedType: String?
    var tmpVideoOuputURL: URL?
    let kStickerImageViewInitWidth = 150.0
    
    var cameraType:String = String()
    var recordedVideoType:String = String()
    
    typealias StickerVideoOutputCompletion = (_ outputUrl: URL, _ error: Error?) -> Void
    
    @IBOutlet var swipeOutlet: UIView!
    @IBOutlet var segmentedStickersOutlet: UISegmentedControl!
    @IBOutlet var textFieldStickers: UITextField!
    @IBOutlet var saveOutlet: UIButton!
    @IBOutlet var hideView: UIView!
    @IBOutlet var heightUIView: NSLayoutConstraint!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var videoPreview: UIView!
    @IBOutlet var viewUp: UIView!
    @IBOutlet weak var canvasView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedType = "Normal" // Initial SetUp
        addInitialUIChanges()
        addGestureRecognizers()
//        interstitial = createAndLoadInterstitial()
        segmentedStickersOutlet.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        self.segmentedStickersOutlet.addTarget(self, action: #selector(self.segmentedControlForStickers(_ :)), for: .valueChanged)
        print("camera type@ addstick viewdid : \(self.cameraType)")
        print("recorded Video Type : \(self.recordedVideoType)")
    }
    override func viewWillAppear(_ animated: Bool) {
        addInitialUIChanges()
        addGestureRecognizers()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    func addInitialUIChanges(){
        self.setupVideoPlayer()
        saveOutlet.layer.cornerRadius = 15.0
        saveOutlet.layer.borderColor = UIColor.black.cgColor
        saveOutlet.layer.borderWidth = 1.0
        self.textFieldStickers.layer.borderColor = UIColor.black.cgColor
        self.textFieldStickers.layer.borderWidth = 1.0
        self.textFieldStickers.layer.cornerRadius = 15
        swipeOutlet.isHidden = true
    }
    func setupVideoPlayer(){
//        self.player.externalPlaybackVideoGravity = AVLayerVideoGravityResizeAspectFill
        if let videoUrl:URL = self.videoUrlFromLocalVideo {
//            self.player = AVPlayer(url: videoUrl)
//            self.avpController = AVPlayerViewController()
//            self.avpController.player = self.player
//            self.avpController.showsPlaybackControls = false
//            avpController.view.frame = view.bounds
//            self.videoPreview.layer.masksToBounds = true
//            self.addChildViewController(avpController)
//            self.videoPreview.addSubview(avpController.view)
            
            self.player = AVPlayer(url: videoUrl)
            self.avpController = AVPlayerViewController()
            self.avpController.player = self.player
            self.avpController.showsPlaybackControls = false
            avpController.view.frame = self.videoPreview.bounds
            avpController.view.layer.masksToBounds = false
            self.videoPreview.layer.masksToBounds = true
            self.addChildViewController(avpController)
            self.videoPreview.addSubview(avpController.view)
        }
    }
    func addGestureRecognizers(){
        addSwipeGesture()
        addTapGesture()
        
    }
    func setupTempVideoOutputUrl() {
        if let url = tmpVideoOuputURL{
            tmpVideoOuputURL =  url
        }
        //        let tempVideoDir: String = getTempVideoOutputDirectory()
        //        let tempVideoFullPath: String = URL(fileURLWithPath: tempVideoDir).appendingPathComponent(kTempVideoFileName).absoluteString
        //        tmpVideoOuputURL = URL(fileURLWithPath: tempVideoFullPath)
    }
    func videoExportedAlertAndNavigation(){
        let alertController = UIAlertController(title: "Your video was successfully saved", message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
            self.present(viewController, animated: true)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func downloadImageFromWeb(s : String, imageView: UIImageView){
        let imageUrl = URL(string: s)
        imageView.kf.setImage(with: imageUrl)
    }
    func addSwipeGesture(){
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
        self.swipeOutlet.addGestureRecognizer(swipeGestureRecognizer)
    }
    func addTapGesture(){
        textFieldStickers.isHidden = false
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(tapAction(tap:)))
        //  tapGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
        self.swipeOutlet.addGestureRecognizer(tapGestureRecognizer)
    }
    func swipeAction(swipe: UISwipeGestureRecognizer){
        print("swipe happened")
        self.heightUIView.constant = 0 + self.viewUp.frame.size.height
        textFieldStickers.isHidden = false
        swipeOutlet.isHidden = true
    }
    func tapAction(tap: UISwipeGestureRecognizer){
        print("tap happened")
        self.heightUIView.constant = 0 + self.viewUp.frame.size.height
        textFieldStickers.isHidden = false
        swipeOutlet.isHidden = true
        
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //textfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder
        UIView.animate(withDuration: 0.50, animations: {
            self.heightUIView.constant = 250
            //    self.viewUp.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.50, animations: {
            //Set x position what ever you want
            textField.resignFirstResponder()
            self.heightUIView.constant = 0
            //   self.viewUp.layoutIfNeeded()
        }, completion: nil)
    }
    
    //collection view delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch selectedType {
        case "Normal"?:
            if imagesArrayNormal.count == 0{
                textFieldStickers.placeholder = "No stickers Found"
            }else{
                textFieldStickers.placeholder = "Stickers"
            }
            return imagesArrayNormal.count
        case "Business"?:
            print(imagesArrayBussiness)
            if imagesArrayBussiness.count == 0{
                textFieldStickers.placeholder = "No stickers Found"
            }else{
                textFieldStickers.placeholder = "Stickers"
            }
            return imagesArrayBussiness.count
        case "Search"?:
            print("StickersSearchImplemented")
            return searchArray.count
        default:
            return imagesArrayNormal.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! MyCollectionViewCell
        cell.activityIndicator.startAnimating()
        switch selectedType {
        case "Normal"?:
            downloadImageFromWeb(s: imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("normal stickers loaded")
           // cell.activityIndicator.stopAnimating()
            break
        case "Business"?:
            print("biz stik url = \(imagesArrayBussiness[indexPath.row])")
            downloadImageFromWeb(s: imagesArrayBussiness[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("business stickers loaded")
         // cell.activityIndicator.stopAnimating()
            break
        case "Search"?:
            downloadImageFromWeb(s: searchArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: cell.myImageView!)
            print("search stickers loaded")
           // cell.activityIndicator.stopAnimating()
            break
        default:
            self.downloadImageFromWeb(s: (imagesArrayNormal[indexPath.row]), imageView: cell.myImageView)
           // cell.activityIndicator.stopAnimating()
            
        }
        cell.activityIndicator.stopAnimating()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: self.view.frame.size.width/5, height: self.view.frame.size.width/5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.50, animations: {
            self.heightUIView.constant = self.heightUIView.constant - (self.viewUp.frame.size.height)
            self.swipeOutlet.isHidden = false
            self.textFieldStickers.isHidden = true
            self.textFieldStickers.resignFirstResponder()
            //  self.viewUp.layoutIfNeeded()
        }, completion: nil)
        //  let draggableview = DraggableStickerView()
        // draggableview.frame =  CGRect(x: 100, y: 100, width: 90, height: 130)
        // draggableview.isUserInteractionEnabled = true
        let selectedSticker = UIImageView()
        selectedSticker.frame =  CGRect(x: 0, y: 30, width: 150, height: 150)
//        selectedSticker.backgroundColor = UIColor.red
        switch selectedType {
        case "Normal"?:
            downloadImageFromWeb(s: imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: selectedSticker)
        case "Business"?:
            downloadImageFromWeb(s: imagesArrayBussiness[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, imageView: selectedSticker)
        case "Search"?:
            self.downloadImageFromWeb(s: (searchArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: selectedSticker)
        default:
            self.downloadImageFromWeb(s: (imagesArrayNormal[indexPath.row].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), imageView: selectedSticker)
        }
        print(selectedSticker)
        selectedSticker.isUserInteractionEnabled = true
        selectedSticker.isMultipleTouchEnabled = true
        //        draggableview.addSubview(selectedSticker)
        //        draggableview.backgroundColor = UIColor.clear
        //        self.videoPreview.addSubview(draggableview)
        newDrag = selectedSticker.toImage()
        self.addAStickerToView(selectedImage:selectedSticker.image!)
    }
    func addAStickerToView(selectedImage:UIImage){
        
        let posX: CGFloat = (CGFloat((self.videoPreview?.bounds.size.width)!) - CGFloat(kStickerImageViewInitWidth)) / 2
        let posY: CGFloat = (CGFloat((self.videoPreview?.bounds.size.height)!) - CGFloat(kStickerImageViewInitWidth)) / 2
        
        let stickerView = StickerImageView(frame: CGRect(x: posX, y: posY, width: CGFloat(kStickerImageViewInitWidth), height: CGFloat(kStickerImageViewInitWidth)))
        stickerView.image = selectedImage
        self.videoPreview?.addSubview(stickerView)
    }
    
    func segmentedControlForStickers(_ sender: UISegmentedControl){
        switch segmentedStickersOutlet.selectedSegmentIndex{
        case 0:
            selectedType = "Normal"
            self.collectionView.reloadData()
        case 1:
            selectedType = "Business"
            self.collectionView.reloadData()
        default:
            selectedType = "Normal"
            self.collectionView.reloadData()
        }
    }
    func saveSquareVideoFromFrontCameraRecorded(){
        //Create Layers for Video Output
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        
        let outputURL:URL = (self.videoUrlFromLocalVideo)!
        print("outputURL : \(outputURL)")
        
        //Setting Up Video Composition
        let videoAsset = AVURLAsset(url: self.videoUrlFromLocalVideo!, options: nil)
        let clipVideoTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
        
        let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(1, 30)
        
        //video  render size
        videoComposition.renderSize = CGSize(width: clipVideoTrack.naturalSize.height, height: (clipVideoTrack.naturalSize.height))
        
        //create a video instruction
        let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30))
        
        // transformer is applied to set the video in portrait otherwise it is rotated by 90 degrees
        let transformer: AVMutableVideoCompositionLayerInstruction =
            AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let t1: CGAffineTransform = CGAffineTransform(translationX: clipVideoTrack.naturalSize.height, y: 0)
        
        
        // Calculate rotation angle
        var angle:CGFloat = CGFloat()
        let deviceOrientation =  UIDevice.current.orientation
        
        switch (deviceOrientation) {
        case .portraitUpsideDown:
            angle = CGFloat(Double.pi)
            break;
        case .landscapeLeft:
            angle = CGFloat(Double.pi/2)
            break;
        case .landscapeRight:
            angle = -CGFloat(Double.pi/2)
            break;
        default:
            angle = 0
            break;
        }
        print("deviceOrientation : \(deviceOrientation.rawValue) and angle: \(angle)")
        
        let t2: CGAffineTransform = t1.rotated(by: CGFloat(Double.pi/2))
        let finalTransform: CGAffineTransform = t2
        //transformer.setTransform(finalTransform, at: kCMTimeZero)
        
        instruction.layerInstructions = NSArray(object: transformer) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // 2 - set up the parent layer
        let newVideoSize = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0].naturalSize
        print("videoComposition.renderSize : \(videoComposition.renderSize)")
        parentLayer.frame = CGRect(x: 0, y: 0, width: videoComposition.renderSize.height, height: videoComposition.renderSize.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: videoComposition.renderSize.height, height: videoComposition.renderSize.height)
        print("videoComposition.renderSize : \(videoComposition.renderSize)")
        
        parentLayer.addSublayer(videoLayer)
        
        let widthFactor =  videoLayer.frame.size.width/self.videoPreview.frame.size.width
        let heightFactor = videoLayer.frame.size.height/self.videoPreview.frame.size.height
        
        for subview: UIView in videoPreview.subviews {
            
            if (subview is StickerImageView) {
                print("yes its a stickerImageView")
                let stickerImageView = subview as? StickerImageView
                print("imgView Frame before transform : \(String(describing: stickerImageView?.frame)) ")
                print("stickerImageView.updatedFrame : \(String(describing: stickerImageView?.updatedFrame))")
                let stickerFrame:CGRect = (stickerImageView?.superview?.convert((stickerImageView?.frame)!, to: nil))!
                print("stickerFrame: \(stickerFrame)")
                
                //height and width factors
                let newImageX = (stickerImageView?.center.x)! * widthFactor
                let newImageY =  (stickerImageView?.center.y)! * heightFactor
                
                print("newImageX : \(newImageX) + newImageY : \(newImageY)")
                
                let updatedFrameForVideo:CGRect = CGRect(x :(stickerImageView?.bounds.minX)!,y:(stickerImageView?.bounds.minY)!,width: (stickerImageView?.bounds.width)! * widthFactor, height: (stickerImageView?.bounds.height)!)
                
                let imageLayer = CALayer()
                imageLayer.contents = stickerImageView?.image?.cgImage
                //imageLayer.frame = (stickerImageView?.bounds)!
                imageLayer.bounds =  updatedFrameForVideo
                imageLayer.opacity = 1
                imageLayer.contentsGravity = kCAGravityResizeAspectFill
                
                let t: CGAffineTransform = (stickerImageView?.transform)!
                imageLayer.masksToBounds = false
                imageLayer.transform = CATransform3DMakeAffineTransform(t)
                
                if let _:CGRect =  stickerFrame{
                  //  imageLayer.position = CGPoint(x: newImageX, y: parentLayer.frame.size.height - newImageY)
                    imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.size.height-newImageY) * 1.35)
                    print("imageLayer.position : \(imageLayer.position)")
                }
                parentLayer.addSublayer(imageLayer)
            }
        }
        
        // 3 - apply magic
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        
        
        //4. Saving it
        let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        
        let timeStamp = NSDate().timeIntervalSince1970
        let exportPath = NSTemporaryDirectory().appendingFormat("/video_\(timeStamp).mov")
        let exportURL = URL(fileURLWithPath: exportPath)
        
        print("exportURL : \(exportURL)")
        exporter?.outputURL =  exportURL
        exporter!.videoComposition = videoComposition
        exporter?.outputFileType = AVFileTypeMPEG4
        exporter?.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, videoAsset.duration)
        exporter?.timeRange = range
        
        print("exporter?.outputURL : \(String(describing: exporter?.outputURL))")
        exporter?.exportAsynchronously(completionHandler: {() -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                self.view.hideToastActivity()    //sravani
                if exporter?.status == .completed {
                    //  handler(outputUrl, nil)
                    print(exporter?.status.rawValue)
                    print("export completed")
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: (exporter?.outputURL)!)
                    }) { saved, error in
                        if saved {
                            print("video saved to photos")
                            self.videoExportedAlertAndNavigation()
                        }else {
                            print("video not saved to photos")
                        }
                    }
                }
                else if exporter?.status == .failed {
                    print("export failed : \(String(describing: exporter?.error))")
                    
                }
                else if exporter?.status == .cancelled{
                    print("export cancelled")
                }
                else {
                    print(exporter?.error)
                }
            })
        })
    }
    
    func saveSquareVideoFromRearCameraRecorded(){
            //Create Layers for Video Output
            let parentLayer = CALayer()
            let videoLayer = CALayer()
            
            let outputURL:URL = (self.videoUrlFromLocalVideo)!
            print("outputURL : \(outputURL)")
            
            //Setting Up Video Composition
            let videoAsset = AVURLAsset(url: self.videoUrlFromLocalVideo!, options: nil)
            let clipVideoTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
            
            let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
            videoComposition.frameDuration = CMTimeMake(1, 30)
            
            //video  render size
            videoComposition.renderSize = CGSize(width: clipVideoTrack.naturalSize.height, height: (clipVideoTrack.naturalSize.height))
            
            //create a video instruction
            let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
            instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30))
            
            // transformer is applied to set the video in portrait otherwise it is rotated by 90 degrees
            let transformer: AVMutableVideoCompositionLayerInstruction =
                AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
            let t1: CGAffineTransform = CGAffineTransform(translationX: clipVideoTrack.naturalSize.height, y: 0)
            
            
            // Calculate rotation angle
            var angle:CGFloat = CGFloat()
            let deviceOrientation =  UIDevice.current.orientation
            
            let t2: CGAffineTransform = t1.rotated(by: CGFloat(Double.pi/2))
            let finalTransform: CGAffineTransform = t2
           // transformer.setTransform(finalTransform, at: kCMTimeZero)
            
            instruction.layerInstructions = NSArray(object: transformer) as! [AVVideoCompositionLayerInstruction]
            videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
            
            // 2 - set up the parent layer
            let newVideoSize = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0].naturalSize
            print("videoComposition.renderSize : \(videoComposition.renderSize)")
            parentLayer.frame = CGRect(x: 0, y: 0, width: videoComposition.renderSize.height, height: videoComposition.renderSize.height)
            videoLayer.frame = CGRect(x: 0, y: 0, width: videoComposition.renderSize.height, height: videoComposition.renderSize.height)
            print("videoComposition.renderSize : \(videoComposition.renderSize)")
            
            parentLayer.addSublayer(videoLayer)
            
            let widthFactor =  videoLayer.frame.size.width/self.videoPreview.frame.size.width
            let heightFactor = videoLayer.frame.size.height/self.videoPreview.frame.size.height
            
            for subview: UIView in videoPreview.subviews {
                
                if (subview is StickerImageView) {
                    print("yes its a stickerImageView")
                    let stickerImageView = subview as? StickerImageView
                    print("imgView Frame before transform : \(stickerImageView?.frame) ")
                    print("stickerImageView.updatedFrame : \(stickerImageView?.updatedFrame)")
                    let stickerFrame:CGRect = (stickerImageView?.superview?.convert((stickerImageView?.frame)!, to: nil))!
                    print("stickerFrame: \(stickerFrame)")
                    
                    //height and width factors
                    let newImageX = (stickerImageView?.center.x)! * widthFactor
                    let newImageY =  (stickerImageView?.center.y)! * heightFactor

                    print("newImageX : \(newImageX) + newImageY : \(newImageY)")
                    
                    let updatedFrameForVideo:CGRect = CGRect(x :(stickerImageView?.bounds.minX)!,y:(stickerImageView?.bounds.minY)!,width: (stickerImageView?.bounds.width)! * widthFactor, height: (stickerImageView?.bounds.height)!)
                    
                    let imageLayer = CALayer()
                    imageLayer.contents = stickerImageView?.image?.cgImage
                    //imageLayer.frame = (stickerImageView?.bounds)!
                    imageLayer.bounds =  updatedFrameForVideo
                    imageLayer.opacity = 1
                    imageLayer.contentsGravity = kCAGravityResizeAspectFill
                    
                    let t: CGAffineTransform = (stickerImageView?.transform)!
                    imageLayer.masksToBounds = false
                    imageLayer.transform = CATransform3DMakeAffineTransform(t)
                    
                    if let emojiFrame:CGRect =  stickerFrame{
                        //imageLayer.position = CGPoint(x: newImageX, y: parentLayer.frame.size.height - newImageY)
                        imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.size.height-newImageY) * 1.3)
                        print("imageLayer.position : \(imageLayer.position)")
                    }
                    parentLayer.addSublayer(imageLayer)
                }
            }
            
            // 3 - apply magic
            videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
            
            
            //4. Saving it
            let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
            
            let timeStamp = NSDate().timeIntervalSince1970
            let exportPath = NSTemporaryDirectory().appendingFormat("/video_\(timeStamp).mov")
            let exportURL = URL(fileURLWithPath: exportPath)
            
            print("exportURL : \(exportURL)")
            exporter?.outputURL =  exportURL
            exporter!.videoComposition = videoComposition
            exporter?.outputFileType = AVFileTypeMPEG4
            exporter?.shouldOptimizeForNetworkUse = true
            let start = CMTimeMakeWithSeconds(0.0, 0)
            let range = CMTimeRangeMake(start, videoAsset.duration)
            exporter?.timeRange = range
            
            print("exporter?.outputURL : \(exporter?.outputURL)")
            
            exporter?.exportAsynchronously(completionHandler: {() -> Void in
                DispatchQueue.main.async(execute: {() -> Void in
                    self.view.hideToastActivity()
                    if exporter?.status == .completed {
                        //  handler(outputUrl, nil)
                        print(exporter?.status.rawValue)
                        print("export completed")
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: (exporter?.outputURL)!)
                        }) { saved, error in
                            if saved {
                                print("video saved to photos")
                                self.videoExportedAlertAndNavigation()
                            }else {
                                print("video not saved to photos")
                            }
                        }
                    }
                    else if exporter?.status == .failed {
                        print("export failed : \(exporter?.error)")
                    }
                    else if exporter?.status == .cancelled{
                        print("export cancelled")
                    }
                    else {
                        print(exporter?.error)
                    }
                })
            })
            
        }
    func savePortraitVideoFromFrontCameraRecorded(){
        
        //Create Layers for Video Output
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        
        let outputURL:URL = (self.videoUrlFromLocalVideo)!
        print("outputURL : \(outputURL)")
        
        //Setting Up Video Composition
        let videoAsset = AVURLAsset(url: self.videoUrlFromLocalVideo!, options: nil)
        let clipVideoTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
        
        let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(1, 30)
        videoComposition.renderSize = CGSize(width: 720, height: 720)
        
        
        let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(180, 30))
        
        // transformer is applied to set the video in portrait otherwise it is rotated by 90 degrees
        let transformer: AVMutableVideoCompositionLayerInstruction =
            AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let t1: CGAffineTransform = CGAffineTransform(translationX: clipVideoTrack.naturalSize.height, y: 0)
        let t2: CGAffineTransform = t1.rotated(by: CGFloat(Double.pi/2))
        let finalTransform: CGAffineTransform = t2
        transformer.setTransform(clipVideoTrack.preferredTransform, at: kCMTimeZero)
       // transformer.setTransform(finalTransform, at: kCMTimeZero)
        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]
        
       
        //instruction.layerInstructions = NSArray(object: transformer) as! [AVVideoCompositionLayerInstruction]
        // videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // 2 - set up the parent layer
        let newVideoSize = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0].naturalSize
        print("videoComposition.renderSize : \(videoComposition.renderSize)")
        parentLayer.frame = CGRect(x: 0, y: 0, width: videoComposition.renderSize.height, height: videoComposition.renderSize.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: videoComposition.renderSize.height, height: videoComposition.renderSize.height)
        print("videoComposition.renderSize : \(videoComposition.renderSize)")
        
        parentLayer.addSublayer(videoLayer)
        print("videoPreview.subviews : \(videoPreview.subviews.count)")
        
        print("videoPreview.frame : \(self.videoPreview.frame)")
        print("videoLayer.frame :\(videoLayer.frame) + parentLayer.frame:\(parentLayer.frame) ")
        
        let widthFactor =  videoLayer.frame.size.width/self.videoPreview.frame.size.width
        let heightFactor = videoLayer.frame.size.height/self.videoPreview.frame.size.height
        
        for subview: UIView in videoPreview.subviews {
            
            if (subview is StickerImageView) {
                print("yes its a stickerImageView")
                let stickerImageView = subview as? StickerImageView
                print("imgView Frame before transform : \(stickerImageView?.frame) ")
                print("stickerImageView.updatedFrame : \(stickerImageView?.updatedFrame)")
                let stickerFrame:CGRect = (stickerImageView?.superview?.convert((stickerImageView?.frame)!, to: nil))!
                print("stickerFrame: \(stickerFrame)")
                
                //height and width factors
                let newImageX = (stickerImageView?.center.x)! * widthFactor
                let newImageY =  (stickerImageView?.center.y)! * heightFactor
                print("newImageX : \(newImageX) + newImageY : \(newImageY)")
                
                let updatedFrameForVideo:CGRect = CGRect(x :(stickerImageView?.bounds.minX)!,y:(stickerImageView?.bounds.minY)!,width: (stickerImageView?.bounds.width)! * widthFactor, height: (stickerImageView?.bounds.height)!)
                
                let imageLayer = CALayer()
                imageLayer.contents = stickerImageView?.image?.cgImage
                //imageLayer.frame = (stickerImageView?.bounds)!
                imageLayer.bounds =  updatedFrameForVideo
                imageLayer.opacity = 1
                imageLayer.contentsGravity = kCAGravityResizeAspectFill
                
                let t: CGAffineTransform = (stickerImageView?.transform)!
                imageLayer.masksToBounds = false
                imageLayer.transform = CATransform3DMakeAffineTransform(t)
                
                if let emojiFrame:CGRect =  stickerFrame{
                    if newImageY < (parentLayer.frame.height/2){
                        print("newImageY : \(newImageY) + \(parentLayer.frame.height/2)")
                        imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.height - newImageY) * 1.02)
                    }
                    else {
                        print("@ else newImageY : \(newImageY) + \(parentLayer.frame.height/2)")
                        imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.height - newImageY) * 1.02)
                        
                    }
                    
                   // imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.size.height - newImageY) * 1.02) //1.05 was before
                    print("imageLayer.position : \(imageLayer.position)")
                }
                parentLayer.addSublayer(imageLayer)
            }
        }
        
        // 3 - apply magic
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        print("videoComposition : \(videoComposition)")
        print("export asset :\(videoAsset)")
        
        
        //4. Saving it
        let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        let timeStamp = NSDate().timeIntervalSince1970
        let exportPath = NSTemporaryDirectory().appendingFormat("/video_\(timeStamp).mov")
        let exportURL = URL(fileURLWithPath: exportPath)
        print("exportURL : \(exportURL)")
        
        // exporter?.outputURL = exportURL
        exporter?.outputURL =  exportURL
        exporter!.videoComposition = videoComposition
        exporter?.outputFileType = AVFileTypeMPEG4
        exporter?.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, videoAsset.duration)
        exporter?.timeRange = range
        
        print("exporter?.outputURL : \(exporter?.outputURL)")
        exporter?.exportAsynchronously(completionHandler: {() -> Void in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.view.hideToastActivity()  //sravani
                if exporter?.status == .completed {
                    //  handler(outputUrl, nil)
                    print(exporter?.status.rawValue)
                    print("export completed")
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: (exporter?.outputURL)!)
                    }) { saved, error in
                        if saved {
                            print("video saved to photos")
                            self.videoExportedAlertAndNavigation()
                        }else {
                            print("video not saved to photos")
                        }
                    }
                    
                }
                else if exporter?.status == .failed {
                    print("export failed : \(exporter?.error)")
                    
                }
                else if exporter?.status == .cancelled{
                    print("export cancelled")
                }
                else {
                    print(exporter?.error)
                }
            })
        })
        
    }
    func savePortraitVideoFromRearCameraRecorded()
    {
        //Create Layers for Video Output
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        
        let outputURL:URL = (self.videoUrlFromLocalVideo)!
        print("outputURL : \(outputURL)")
        
        //Setting Up Video Composition
        let videoAsset = AVURLAsset(url: self.videoUrlFromLocalVideo!, options: nil)
        let clipVideoTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
        
        let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(1, 60)
        
        //video  render size
        videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.height), height: (clipVideoTrack.naturalSize.width))
        
        let videoRect = CGRect(x: 0.0, y: 0.0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        
        videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.height), height: (clipVideoTrack.naturalSize.width))
        parentLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        videoLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        print("videoComposition.renderSize : \(videoComposition.renderSize)")
        
        let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30))
        
        // transformer is applied to set the video in portrait otherwise it is rotated by 90 degrees
        let transformer: AVMutableVideoCompositionLayerInstruction =
            AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let t1: CGAffineTransform = CGAffineTransform(translationX: clipVideoTrack.naturalSize.height, y: 0)
        let t2: CGAffineTransform = t1.rotated(by: CGFloat(M_PI_2))
        let finalTransform: CGAffineTransform = t2
        
        transformer.setTransform(clipVideoTrack.preferredTransform, at: kCMTimeZero)
        //transformer.setTransform(finalTransform, at: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: transformer) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // 2 - set up the parent layer
        let videoSize = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0].naturalSize
        parentLayer.addSublayer(videoLayer)
        print("videoPreview.subviews : \(videoPreview.subviews.count)")
        
        print("videoPreview.frame : \(self.videoPreview.frame)")
        print("videoLayer.frame :\(videoLayer.frame) + parentLayer.frame:\(parentLayer.frame) ")
        
        let widthFactor =  videoLayer.frame.size.width/self.videoPreview.frame.size.width
        let heightFactor = videoLayer.frame.size.height/self.videoPreview.frame.size.height
        
        for subview: UIView in videoPreview.subviews {
            
            if (subview is StickerImageView) {
                print("yes its a stickerImageView")
                let stickerImageView = subview as? StickerImageView
                print("imgView Frame before transform : \(stickerImageView?.frame) ")
                print("stickerImageView.updatedFrame : \(stickerImageView?.updatedFrame)")
                let stickerFrame:CGRect = (stickerImageView?.superview?.convert((stickerImageView?.frame)!, to: nil))!
                print("stickerFrame: \(stickerFrame)")
                
                //height and width factors
                let newImageX = (stickerImageView?.center.x)! * widthFactor
                let newImageY =  (stickerImageView?.center.y)! * heightFactor
                print("newImageX : \(newImageX) + newImageY : \(newImageY)")
                
                let updatedFrameForVideo:CGRect = CGRect(x :(stickerImageView?.bounds.minX)!,y:(stickerImageView?.bounds.minY)!,width: (stickerImageView?.bounds.width)! * widthFactor, height: (stickerImageView?.bounds.height)!)
                
                let imageLayer = CALayer()
                imageLayer.contents = stickerImageView?.image?.cgImage
                //imageLayer.frame = (stickerImageView?.bounds)!
                imageLayer.bounds =  updatedFrameForVideo
                imageLayer.opacity = 1
                imageLayer.contentsGravity = kCAGravityResizeAspectFill
                
                let t: CGAffineTransform = (stickerImageView?.transform)!
                imageLayer.masksToBounds = false
                imageLayer.transform = CATransform3DMakeAffineTransform(t)
                
                if let emojiFrame:CGRect =  stickerFrame{
                    
                    if newImageY < (parentLayer.frame.height/2){
                        print("newImageY : \(newImageY) + \(parentLayer.frame.height/2)")
                        imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.height - newImageY) * 1.02)
                    }
                    else {
                        print("@ else newImageY : \(newImageY) + \(parentLayer.frame.height/2)")
                        imageLayer.position = CGPoint(x: newImageX, y: (parentLayer.frame.height - newImageY) * 1.02)
                        
                    }
                    //imageLayer.position = CGPoint(x: newImageX, y: parentLayer.frame.origin.y + newImageY) //1.05 was before
                    print("imageLayer.position : \(imageLayer.position)")
                }
                parentLayer.addSublayer(imageLayer)
            }
        }
        
        // 3 - apply magic
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        print("videoComposition : \(videoComposition)")
        print("export asset :\(videoAsset)")
        
        
        //4. Saving it
        let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        let timeStamp = NSDate().timeIntervalSince1970
        let exportPath = NSTemporaryDirectory().appendingFormat("/video_\(timeStamp).mov")
        let exportURL = URL(fileURLWithPath: exportPath)
        print("exportURL : \(exportURL)")
        
        // exporter?.outputURL = exportURL
        exporter?.outputURL =  exportURL
        exporter!.videoComposition = videoComposition
        exporter?.outputFileType = AVFileTypeMPEG4
        exporter?.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, videoAsset.duration)
        exporter?.timeRange = range
        
        print("exporter?.outputURL : \(exporter?.outputURL)")
        exporter?.exportAsynchronously(completionHandler: {() -> Void in
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.view.hideToastActivity()    //sravani
                if exporter?.status == .completed {
                    //  handler(outputUrl, nil)
                    print(exporter?.status.rawValue)
                    print("export completed")
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: (exporter?.outputURL)!)
                    }) { saved, error in
                        if saved {
                            print("video saved to photos")
                            self.videoExportedAlertAndNavigation()
                        }else {
                            print("video not saved to photos")
                        }
                    }
                    
                }
                else if exporter?.status == .failed {
                    print("export failed : \(exporter?.error)")
                    
                }
                else if exporter?.status == .cancelled{
                    print("export cancelled")
                }
                else {
                    print(exporter?.error)
                }
            })
//            self.view.hideToastActivity()
        })
    }
    
    func image(_ image: UIImage, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        let alert = UIAlertController(title: "Image Saved", message: "Image successfully saved to Photos library", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController : HomeScreenCameraViewController = storyboard.instantiateViewController(withIdentifier :"homeScreenCameraView") as! HomeScreenCameraViewController
            self.present(viewController, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func requestError(error: Error?) {
        self.view.hideToastActivity()
        self.view.makeToast(message: (error?.localizedDescription)!)
    }
    
    //MARK:-save button clicked - process video with stickers on top layer
    @IBAction func saveVideoWithStickers (_ sender:Any) {
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            self.view.makeToastActivity(message: "Loading..")
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                    switch self.recordedVideoType {
                    case VideoTypeFromRecording.SQUARE_FRONT.rawValue:
                        self.saveSquareVideoFromFrontCameraRecorded()
                        break
                    case VideoTypeFromRecording.SQUARE_REAR.rawValue:
                        self.saveSquareVideoFromRearCameraRecorded()
                        break
                    case VideoTypeFromRecording.PORTRAIT_FRONT.rawValue:
                        self.savePortraitVideoFromFrontCameraRecorded()
                        break
                    case VideoTypeFromRecording.PORTRAIT_REAR.rawValue:
                        self.savePortraitVideoFromRearCameraRecorded()
                        break
                    default:
                        break
                    }
//                    self.view.hideToastActivity()
            }
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.view.makeToastActivity(message: "Loading..")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                            switch self.recordedVideoType {
                            case VideoTypeFromRecording.SQUARE_FRONT.rawValue:
                                self.saveSquareVideoFromFrontCameraRecorded()
                                break
                            case VideoTypeFromRecording.SQUARE_REAR.rawValue:
                                self.saveSquareVideoFromRearCameraRecorded()
                                break
                            case VideoTypeFromRecording.PORTRAIT_FRONT.rawValue:
                                self.savePortraitVideoFromFrontCameraRecorded()
                                break
                            case VideoTypeFromRecording.PORTRAIT_REAR.rawValue:
                                self.savePortraitVideoFromRearCameraRecorded()
                                break
                            default:
                                break
                            }
//                            self.view.hideToastActivity()
                    }
                }
                else {
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
        //
        //        if self.cameraType == "front"{
        //            self.saveSquareVideoFromFrontCameraRecorded()
        //            print("front confirmed")
        //
        //        }
        //        else{
        //            print("cameraType @ save :  \(cameraType)")
        //
        //            //Create Layers for Video Output
        //            let parentLayer = CALayer()
        //            let videoLayer = CALayer()
        //
        //            let outputURL:URL = (self.videoUrlFromLocalVideo)!
        //            print("outputURL : \(outputURL)")
        //
        //            //Setting Up Video Composition
        //            let videoAsset = AVURLAsset(url: self.videoUrlFromLocalVideo!, options: nil)
        //            let clipVideoTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
        //
        //            let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        //            videoComposition.frameDuration = CMTimeMake(1, 60)
        //
        //            //video  render size
        //            videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.height), height: (clipVideoTrack.naturalSize.width))
        //
        //            let videoRect = CGRect(x: 0.0, y: 0.0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //
        //
        //            if isVideoTakenFromGallery == true{
        //                print("taken from gallery -  change render orientation")
        //
        //                if videoRect.size.height > videoRect.size.width {
        //                    print("Portrait mode")
        //                    videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.height), height: (clipVideoTrack.naturalSize.width))
        //                    parentLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //                    videoLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //                }
        //                else if videoRect.size.height < videoRect.size.width {
        //                    print("Landscape mode")
        //                    videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.width), height: (clipVideoTrack.naturalSize.height))
        //                    parentLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //                    videoLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //
        //                }
        //                else {
        //                    print("Square mode")
        //                }
        //
        //
        //            }
        //            else {
        //                print("taken from camera")
        //
        //                if videoRect.size.height > videoRect.size.width {
        //                    print("Portrait mode")
        //                    videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.height), height: (clipVideoTrack.naturalSize.width))
        //                    parentLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //                    videoLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //                }
        //                else if videoRect.size.height < videoRect.size.width {
        //                    print("Landscape mode")
        //                    videoComposition.renderSize = CGSize(width: (clipVideoTrack.naturalSize.width), height: (clipVideoTrack.naturalSize.height))
        //                    parentLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.width, height: clipVideoTrack.naturalSize.height)
        //                    videoLayer.frame = CGRect(x: 0, y: 0, width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        //                }
        //                else {
        //                    print("Square mode")
        //                }
        //
        //            }
        //
        //
        //
        //            print("videoComposition.renderSize : \(videoComposition.renderSize)")
        //
        //            let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        //            instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30))
        //
        //            // transformer is applied to set the video in portrait otherwise it is rotated by 90 degrees
        //            let transformer: AVMutableVideoCompositionLayerInstruction =
        //                AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        //            let t1: CGAffineTransform = CGAffineTransform(translationX: clipVideoTrack.naturalSize.height, y: 0)
        //            let t2: CGAffineTransform = t1.rotated(by: CGFloat(M_PI_2))
        //            let finalTransform: CGAffineTransform = t2
        //
        //            transformer.setTransform(clipVideoTrack.preferredTransform, at: kCMTimeZero)
        //            //transformer.setTransform(finalTransform, at: kCMTimeZero)
        //            instruction.layerInstructions = NSArray(object: transformer) as! [AVVideoCompositionLayerInstruction]
        //            videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        //
        //            // 2 - set up the parent layer
        //            let videoSize = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0].naturalSize
        //
        //            //parent - layer
        //            //let parentLayer = CALayer()
        //            // parentLayer.frame = CGRect(x: 0, y: 0, width: videoSize.height, height: videoSize.width)
        //
        //            //video - layer
        //            //  let videoLayer = CALayer()
        //            // videoLayer.frame = CGRect(x: 0, y: 0, width: videoSize.height, height: videoSize.width)
        //
        //            parentLayer.addSublayer(videoLayer)
        //            print("videoPreview.subviews : \(videoPreview.subviews.count)")
        //
        //            print("videoPreview.frame : \(self.videoPreview.frame)")
        //            print("videoLayer.frame :\(videoLayer.frame) + parentLayer.frame:\(parentLayer.frame) ")
        //
        //            let widthFactor =  videoLayer.frame.size.width/self.videoPreview.frame.size.width
        //            let heightFactor = videoLayer.frame.size.height/self.videoPreview.frame.size.height
        //
        //            for subview: UIView in videoPreview.subviews {
        //
        //                if (subview is StickerImageView) {
        //                    print("yes its a stickerImageView")
        //                    let stickerImageView = subview as? StickerImageView
        //                    print("imgView Frame before transform : \(stickerImageView?.frame) ")
        //                    print("stickerImageView.updatedFrame : \(stickerImageView?.updatedFrame)")
        //                    let stickerFrame:CGRect = (stickerImageView?.superview?.convert((stickerImageView?.frame)!, to: nil))!
        //                    print("stickerFrame: \(stickerFrame)")
        //
        //                    //height and width factors
        //                    let newImageX = (stickerImageView?.center.x)! * widthFactor
        //                    let newImageY =  (stickerImageView?.center.y)! * heightFactor
        //                    print("newImageX : \(newImageX) + newImageY : \(newImageY)")
        //
        //                    let updatedFrameForVideo:CGRect = CGRect(x :(stickerImageView?.bounds.minX)!,y:(stickerImageView?.bounds.minY)!,width: (stickerImageView?.bounds.width)! * widthFactor, height: (stickerImageView?.bounds.height)!)
        //
        //                    let imageLayer = CALayer()
        //                    imageLayer.contents = stickerImageView?.image?.cgImage
        //                    //imageLayer.frame = (stickerImageView?.bounds)!
        //                    imageLayer.bounds =  updatedFrameForVideo
        //                    imageLayer.opacity = 1
        //                    imageLayer.contentsGravity = kCAGravityResizeAspectFill
        //
        //                    let t: CGAffineTransform = (stickerImageView?.transform)!
        //                    imageLayer.masksToBounds = false
        //                    imageLayer.transform = CATransform3DMakeAffineTransform(t)
        //
        //                    if let emojiFrame:CGRect =  stickerFrame{
        //
        //
        //
        //                        imageLayer.position = CGPoint(x: newImageX, y: parentLayer.frame.size.height - newImageY)
        //
        //                        print("imageLayer.position : \(imageLayer.position)")
        //                    }
        //
        //
        //                    parentLayer.addSublayer(imageLayer)
        //                }
        //            }
        //
        //            // 3 - apply magic
        //            videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        //
        //
        //            print("videoComposition : \(videoComposition)")
        //            print("export asset :\(videoAsset)")
        //
        //
        //            //4. Saving it
        //            let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        //
        //            let timeStamp = NSDate().timeIntervalSince1970
        //            let exportPath = NSTemporaryDirectory().appendingFormat("/video_\(timeStamp).mov")
        //            let exportURL = URL(fileURLWithPath: exportPath)
        //
        //            print("exportURL : \(exportURL)")
        //
        //            // exporter?.outputURL = exportURL
        //            exporter?.outputURL =  exportURL
        //            exporter!.videoComposition = videoComposition
        //            exporter?.outputFileType = AVFileTypeMPEG4
        //            exporter?.shouldOptimizeForNetworkUse = true
        //            let start = CMTimeMakeWithSeconds(0.0, 0)
        //            let range = CMTimeRangeMake(start, videoAsset.duration)
        //            exporter?.timeRange = range
        //
        //            print("exporter?.outputURL : \(exporter?.outputURL)")
        //
        //            exporter?.exportAsynchronously(completionHandler: {() -> Void in
        //
        //                DispatchQueue.main.async(execute: {() -> Void in
        //
        //                    if exporter?.status == .completed {
        //                        //  handler(outputUrl, nil)
        //                        print(exporter?.status.rawValue)
        //                        print("export completed")
        //                        PHPhotoLibrary.shared().performChanges({
        //                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: (exporter?.outputURL)!)
        //                        }) { saved, error in
        //                            if saved {
        //                                print("video saved to photos")
        //                                self.videoExportedAlertAndNavigation()
        //                            }else {
        //                                print("video not saved to photos")
        //                            }
        //                        }
        //
        //                    }
        //                    else if exporter?.status == .failed {
        //                        print("export failed : \(exporter?.error)")
        //
        //                    }
        //                    else if exporter?.status == .cancelled{
        //                        print("export cancelled")
        //                    }
        //                    else {
        //                        print(exporter?.error)
        //                    }
        //                })
        //            })
        //
        //        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        print("action triggered")
        if segmentedStickersOutlet.selectedSegmentIndex == 0{
            searchArray.removeAll()
            for (_, element) in normalStickerArray.enumerated(){
                if (element["tag"] as! String).hasPrefix(textFieldStickers.text!){
                    searchArray.append(element["imgurl"] as! String)
                }
            }
        } else if segmentedStickersOutlet.selectedSegmentIndex == 1{
            searchArray.removeAll()
            for (_, element) in businessStickerArray.enumerated(){
                if (element["tag"] as! String).hasPrefix(textFieldStickers.text!){
                    searchArray.append(element["imgurl"] as! String)
                }
            }
        }
        selectedType = "Search"
        collectionView.reloadData()
    }
}

